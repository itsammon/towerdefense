# My project's README

This is a simple tower defense game made for Dr. Deans' class.

The grading criteria is the same as last semester's.

I currently have:
A node.js server, (5pts)
configurable controls, (8pts)
animated turrets and creeps, (9pts)
health bars, (3pts)
radius for tower placement, (5pts)
working creep ai, (6pts)
working tower firing system, (10pts)
menuing system, (? pts)
a square arena.
Game System so far, (10-15pts)

Things left to do:
4 tower types and animations/projectiles,
Different Levels,
Particle systems, (15pts)
Scoring,
Lives,
Other directions for creeps,
Creep death,
Path blocking (Tower's can be placed to block path's currently)

