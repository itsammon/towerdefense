let MAX_HIGH_SCORES = 10;
var fs = require('fs');

let scores = fs.readFileSync('./public/highscores.json', 'utf8');

let highscores = JSON.parse(scores),
    nextId = 1;

if (typeof highscores == 'undefined') {
    highscores = [];
};

// Write highscores down
exports.all = function(request, response) {
    response.writeHead(200, {'content-type': 'application/json'});
    response.end(JSON.stringify(highscores));
};

function addScore(name, score) {
    let inserted = false;
    let scores = null;
    for (let i = 0; i < highscores.length; i++) {
        if (score > highscores[i].score) {
            highscores.splice(i, 0, { name: name, score: score } );
            inserted = true;
            break;
        }
    }
    if (!inserted) {
        highscores[highscores.length] = { name: name, score:score };
    }

    if (highscores.length > MAX_HIGH_SCORES) {
        highscores.splice(MAX_HIGH_SCORES-1,highscores.length-MAX_HIGH_SCORES);
    }

    // Write to external file to save scores 
    scores = JSON.stringify(highscores);

    fs.writeFile("./public/highscores.json", scores, function(err) {
        if(err) {
            return console.log(err);
        }

        console.log("The file was saved!");
    }); 
}

exports.add = function(request, response) {
    addScore(request.query.name, request.query.score);

    response.writeHead(200);
    response.end();
}