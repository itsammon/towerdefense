MyGame.highscores = (function() {
    function addHighScore(name, score) {

        $.ajax({
            url: 'http://localhost:3000/hs/highscores?name=' + name + '&score=' + score,
            type: 'POST',
            error: function() { alert('POST failed'); },
            success: function() {
                showHighScores();
            }
        });
    }

    function showHighScores() {
        $.ajax({
            url: 'http://localhost:3000/hs/highscores',
            cache: false,
            type: 'GET',
            error: function() { alert('GET failed'); },
            success: function(data) {
                var list = $('#id-highscores'),
                    value,
                    text;

                list.empty();
                for (value=0; value < data.length; value++) {
                    text = (data[value].name + ' : ' + data[value].score);
                    list.append($('<li>', { text: text }));
                }
            }
        });
    }

    return {
        addHighScore: addHighScore,
        showHighScores: showHighScores,
    }
}());

MyGame.persistence = (function () {
        'use strict';
        var keybindings = {},
            prevKeybindings = localStorage.getItem('TowerDefense.keybindings');
        var highScores = [],
            previousScores = localStorage.getItem('TowerDefense.keybindings');
        
        // If there are previous scores
        if (previousScores !== null) {
            highScores = JSON.parse(previousScores)
        }

        if (prevKeybindings !== null) {
            keybindings = JSON.parse(prevKeybindings);
        }

        function setControl(action, keycode) {
            keybindings[action] = keycode;

            localStorage['TowerDefense.keybindings'] = JSON.stringify(keybindings);
        }

        function getControls() {
            return keybindings;
        }

        function resetControls() {
            keybindings = {};

            localStorage['TowerDefense.keybindings'] = JSON.stringify(keybindings);
        }

        function add(name, score) {
            let inserted = false;
            for (let i = 0; i < highScores.length; i++) {
                if (score > highScores[i].score) {
                    highScores.splice(i, 0, { name: name, score: score } );
                    inserted = true;
                    break;
                }
            }
            if (!inserted) {
                highScores[highScores.length] = { name: name, score:score };
            }

            if (highScores.length > MAX_HIGH_SCORES) {
                highScores.splice(MAX_HIGH_SCORES-1,highScores.length-MAX_HIGH_SCORES);
            }

            localStorage['TowerDefense.highScores'] = JSON.stringify(highScores);
        }

        function remove(key) {
            delete highScores[key];
            localStorage['TowerDefense.highScores'] = JSON.stringify(highScores);
        }

        function reset() {
            highScores.length = 0;
            localStorage['TowerDefense.highScores'] = JSON.stringify(highScores);
        }

        function report() {
            var scores;

            scores = '';
            for (let i = 0; i < highScores.length; i++) {
                scores += '<h3>' + highScores[i].name + ': ' + highScores[i].score + '</h3>';
            }
            return scores;
        }

        return {
            setControl: setControl,
            getControls: getControls,
            resetControls: resetControls,
            add: add,
            remove: remove,
            reset: reset,
            report: report
        };
}())

MyGame.main = (function(screens) {
    'use strict';

    function showScreen(screenId) {
        let screen = 0;
        let active = null;

        active = document.getElementsByClassName('active');
        for (screen = 0; screen < active.length; screen++) {
            active[screen].classList.remove('active');
        }

        screens[screenId].run();

        document.getElementById(screenId).classList.add('active');
    }

    function initialize() {
        let screen = null;

        for (screen in screens) {
            if (screens.hasOwnProperty(screen)) {
                screens[screen].initialize();
            }
        }

        // Start up the audio system
        Audio.initialize();

        showScreen('mainMenu');
    }

    return {
        initialize: initialize,
        showScreen: showScreen
    };
}(MyGame.screens));