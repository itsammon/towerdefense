'use strict';

let Audio = (function () {

    function loadSound(sound, source) {

        // Test Code
        /*
		sound.addEventListener('canplay', function() {
			console.log(`${source} is ready to play`);
		});
		sound.addEventListener('play', function() {
			console.log(`${source} started playing`);
		});
		sound.addEventListener('canplaythrough', function() {
			console.log(`${source} can play through`);
		});
        sound.addEventListener('progress', function() {
			console.log(`${source} progress in loading`);
		});
		sound.addEventListener('timeupdate', function() {
			console.log(`${source} time update: ${this.currentTime}`);
		});
        */
        
        // Reset sound when it finishes playing
        sound.addEventListener('ended', reset, false);
        sound.volume = .25;
	}
    
    function reset() {
        this.currentTime = 0;
    }

    function repeat() {
        this.currentTime = 0;
        this.play();
    }

    // This function is to allow looping sounds
    function repeatSound(sound) {
        sound.addEventListener('ended', repeat, false);
    }

    function stopRepeatSound(sound) {
        sound.removeEventListener('ended', repeat, false);
    }

    function stopMusic(music) {
        if (music !== null && MyGame.assets[music] !== null) {
            if (music.substr(music.lastIndexOf('.') + 1) === 'mp3') {
                stopRepeatSound(MyGame.assets[music]);
            }
        }
        stopSound(music);
    }

    function stopSound(sound) {
        if (sound !== null && MyGame.assets[sound] !== null) {
            if (sound.substr(sound.lastIndexOf('.') + 1) === 'mp3') {
                MyGame.assets[sound].pause();
                MyGame.assets[sound].currentTime = 0;
            }
        }

    }

    function playMusic(music) {
        if (music !== null && MyGame.assets[music] !== null) {
            if (music.substr(music.lastIndexOf('.') + 1) === 'mp3') {
                repeatSound(MyGame.assets[music]);
                MyGame.assets[music].play();
            }
        }
    }

    function playSound(sound) {
        if (sound !== null && MyGame.assets[sound] !== null) {
            if (sound.substr(sound.lastIndexOf('.') + 1) === 'mp3') {
                MyGame.assets[sound].currentTime = 0;
                MyGame.assets[sound].play();
            }
        }
    }

    function initialize() {
        let asset = null;
        for (asset in MyGame.assets) {
			let fileExtension = asset.substr(asset.lastIndexOf('.') + 1);	// Source: http://stackoverflow.com/questions/680929/how-to-extract-extension-from-filename-string-in-javascript
            if (fileExtension === 'mp3') {
                loadSound(MyGame.assets[asset], asset);
            }
        }

        playMusic('backgroundMusic.mp3');
    }

    return {
        playSound: playSound,
        initialize: initialize,
    }

})();