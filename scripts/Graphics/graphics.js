'use strict';

let Graphics = (function () {
    let context = null;

    function initialize() {
        console.log('Initializing Graphics');

        let canvas = document.getElementById('canvas');
        context = canvas.getContext('2d');

        CanvasRenderingContext2D.prototype.clear = function() {
            this.save();
            this.setTransform(1,0,0,1,0,0);
            this.clearRect(0,0, canvas.width, canvas.height);
            this.restore();
        };
    }

    function addCanvasEventListener(eventType, handler) {
        canvas.addEventListener(eventType, handler);
    }

    function getCanvasWidth() {
        return canvas.width;
    }

    function getCanvasHeight() {
        return canvas.height;
    }

    function getCanvasBoundingRect() {
        return canvas.getBoundingClientRect();
    }

    function Rectangle(spec) {
        let that = {};

        that.draw = function() {
            context.save();

            context.translate(spec.corner.x + spec.size.w / 2, spec.corner.y + spec.size.h / 2);
            context.rotate(spec.rotation);
            context.translate(-(spec.corner.x + spec.size.w / 2), -(spec.corner.y + spec.size.h / 2));

            context.fillStyle = spec.fillStyle;
            context.fillRect(spec.corner.x, spec.corner.y, spec.size.w, spec.size.h);

            context.strokeStyle = spec.strokeStyle;
            context.strokeRect(spec.corner.x, spec.corner.y, spec.size.w, spec.size.h);

            context.restore();
        }

        return that;
    }

    function Circle(spec) {
        let that = {};
        
        that.draw = function() {
            context.save();

            context.beginPath();
            context.arc(spec.center.x, spec.center.y, spec.radius, 0, 2*Math.PI, false);
            context.fillStyle = spec.fillStyle;
            context.fill();
            context.strokeStyle = spec.strokeStyle;
            context.stroke();

            context.restore();
        }

        return that;
    }

    function Arc(spec) {
        let that = {};
        
        that.draw = function() {
            context.save();

			context.beginPath();
			context.arc(spec.center.x, spec.center.y, spec.radius, spec.rotation - spec.angle / 2, spec.rotation + spec.angle / 2);
			context.fillStyle = spec.fillStyle;
            context.lineTo(spec.center.x, spec.center.y);
			context.fill();

            context.restore();
        }

        return that;
    }

    function Texture(spec) {
        var that = {},
            ready = false,
            image = new Image();

        if (spec.hasOwnProperty('image')) {
            image = MyGame.assets[spec.image];
            ready = true;
        } else if (spec.hasOwnProperty('imageSource')) {
            image.onload = () => {
                ready = true;
            };
            image.src = spec.imageSource;
        }

        that.changeCenter = function(newCenter) {
            spec.center.x = newCenter.x;
            spec.center.y = newCenter.y;
        }

        that.changeSize = function(newWidth, newHeight) {
            spec.width = newWidth;
            spec.height = newHeight;
        }

        that.changeRotation = function(rotation) {
            spec.rotation = rotation;
        }

        that.draw = function() {
            if (ready) {
                context.save();

                context.translate(spec.center.x, spec.center.y);
                context.rotate(spec.rotation);
                context.translate(-spec.center.x, -spec.center.y);

                context.drawImage(
                    image,
                    spec.center.x - spec.width / 2,
                    spec.center.y - spec.height / 2,
                    spec.width, spec.height);

                context.restore();
            }
        }

        return that;
    }

    function Sprite(spec) {
        var that = {},
            ready = false,
            image = new Image();

        if (spec.hasOwnProperty('image')) {
            image = MyGame.assets[spec.image];
            ready = true;
        } else if (spec.hasOwnProperty('imageSource')) {
            image.onload = () => {
                ready = true;
            };
            image.src = spec.imageSource;
        }

        that.changeCenter = function(newCenter) {
            spec.center.x = newCenter.x;
            spec.center.y = newCenter.y;
        }

        that.changeSize = function(newWidth, newHeight) {
            spec.width = newWidth;
            spec.height = newHeight;
        }

        that.changeRotation = function(rotation) {
            spec.rotation = rotation;
        }

        that.draw = function(whichSprite) {
            if (ready) {
                context.save();

                context.translate(spec.center.x, spec.center.y);
                context.rotate(spec.rotation);
                context.translate(-spec.center.x, -spec.center.y);

                context.drawImage(
                    image,
                    whichSprite*spec.spriteDimensions.x,
                    0,
                    spec.spriteDimensions.x,
                    spec.spriteDimensions.y,
                    spec.center.x - spec.width / 2,
                    spec.center.y - spec.height / 2,
                    spec.width, spec.height);

                context.restore();
            }
        }

        return that;
    }

    function drawParticle(spec) {
        if (spec.type === 'text') {
            let text = Graphics.Text({
                text: spec.text,
                font: '10px Arial, sans-serif',
                fill: 'rgba(255,255,0,1)',
                stroke: 'rgba(255,255,0,1)',
                location: {x: spec.center.x, y: spec.center.y },
                rotation: 0
            });
            text.draw();
        } else {
            drawImage(spec);
        }
    }

    // Allow simple drawing of images for particles
    function drawImage(spec) {
		context.save();
		
		context.translate(spec.center.x, spec.center.y);
		context.rotate(spec.rotation);
		context.translate(-spec.center.x, -spec.center.y);
		
		context.drawImage(
			spec.image, 
			spec.center.x - spec.size/2, 
			spec.center.y - spec.size/2,
			spec.size, spec.size);
		
		context.restore();
	}

    // Create text function for rendering text
    function Text(spec) {
        var that = {};

        that.updateRotation = function(angle) {
            spec.rotation += angle;
        }

        function measureTextHeight(spec) {
            context.save();

            context.font = spec.font;
            context.fillStyle = spec.fill;
            context.strokeStyle = spec.stroke;

            var height = context.measureText('m').width;

            context.restore();

            return height;
        }

        function measureTextWidth(spec) {
            context.save();
            
            context.font = spec.font;
            context.fillStyle = spec.fill;
            context.strokeStyle = spec.stroke;

            var width = context.measureText(spec.text).width;

            context.restore();

            return width;
        }

        that.draw = function() {
            context.save();

            context.font = spec.font;
            context.fillStyle = spec.fill;
            context.strokeStyle = spec.stroke;
            context.textBaseline = 'middle';
            context.textAlign = 'center';

            context.translate(spec.location.x, spec.location.y );
            context.rotate(spec.rotation);
            context.translate(-(spec.location.x), -(spec.location.y ));
            context.fillText(spec.text, spec.location.x, spec.location.y);
            context.strokeText(spec.text, spec.location.x, spec.location.y);

            context.restore();
        }

        // Compute public properties for the text
        that.height = measureTextHeight(spec);
        that.width = measureTextWidth(spec);
        that.location = spec.location;

        return that;
    }

    // Function to clear the canvas
    function clear() {
        context.clear();
    }

    function beginRender() {
        context.clear();
    }

    return {
        beginRender: beginRender,
        initialize: initialize,
        drawParticle: drawParticle,
        drawImage: drawImage,
        Text: Text,
        Texture: Texture,
        Sprite: Sprite,
        Rectangle: Rectangle,
        Circle: Circle,
        Arc: Arc,
        addCanvasEventListener: addCanvasEventListener,
        getCanvasWidth: getCanvasWidth,
        getCanvasHeight: getCanvasHeight,
        getCanvasBoundingRect: getCanvasBoundingRect
    }
}());
