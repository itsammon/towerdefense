let ParticleSystem = (function(graphics) {
	'use strict';
	var that = {},
		nextName = 1,	// unique identifier for the next particle
		particles = {}, // Set of all active particles
		effects = {},
		nextEffect = 1;

	//------------------------------------------------------------------
	//
	// This creates one new particle
	//
	//------------------------------------------------------------------
	that.createSphereParticle = function(spec) {
		let image = MyGame.assets[spec.image];
		var p = {
				type: 'sphere',
				image: image,
				size: Random.nextGaussian(10, 4),
                center: {x: spec.center.x, y: spec.center.y},
				direction: Random.nextCircleVector(),
				speed: Random.nextGaussian(spec.speed.mean, spec.speed.stdev), // pixels per second
				rotation: 0,
				lifetime: Random.nextGaussian(spec.lifetime.mean, spec.lifetime.stdev),	// How long the particle should live, in seconds
				alive: 0	// How long the particle has been alive, in seconds
			};
		
		//
		// Ensure we have a valid size - gaussian numbers can be negative
		p.size = Math.max(1, p.size);
		//
		// Same thing with lifetime
		p.lifetime = Math.max(0.01, p.lifetime);
		//
		// Assign a unique name to each particle
		particles[nextName++] = p;
	};

	function createTextParticle(spec) {
		var p = {
				type: 'text',
				text: spec.text,
                center: {x: spec.center.x, y: spec.center.y},
				direction: { x: 0, y: -1 },
				speed: 10, // pixels per second
				rotation: 0,
				lifetime: spec.lifetime,	// How long the particle should live, in seconds
				alive: 0	// How long the particle has been alive, in seconds
			};
		
		// Make sure lifetime is valid
		// Same thing with lifetime
		p.lifetime = Math.max(0.01, p.lifetime);
		//
		// Assign a unique name to each particle
		particles[nextName++] = p;
	};

	that.createEffect = function(spec) {
		var e = {
			duration: spec.duration,
			action: spec.action,
			alive: 0,
		}

		// Do the action on each update
		e.update = function(elapsedTime) {
			e.alive += elapsedTime;
			e.action();
			if (e.alive > e.duration) {
				return true;
			} else {
				return false;
			}
		}

		e.duration = Math.max(0.01, e.duration);

		effects[nextEffect++] = e;
	}

	that.createFireEffect = function(spec) {
		that.createEffect({
			position: spec.position,
			duration: spec.duration,
			action: function() {
				that.createSphereParticle({
					image: 'fire',
					center: {x: spec.position.x, y: spec.position.y },
					speed: { mean: spec.speed.mean, stdev: spec.speed.stdev },
					lifetime: { mean: spec.lifetime.mean, stdev: spec.lifetime.stdev },
				});
			}
		});
	};
	
	that.createSmokeEffect = function(spec) {
		that.createEffect({
			position: spec.position,
			duration: spec.duration,
			action: function() {
				that.createSphereParticle({
					image: 'smoke',
					center: {x: spec.position.x, y: spec.position.y },
					speed: { mean: spec.speed.mean, stdev: spec.speed.stdev },
					lifetime: { mean: spec.lifetime.mean, stdev: spec.lifetime.stdev },
				});
			}
		});
	};

	that.createTextEffect = function(spec) {
		createTextParticle({
			text: spec.text,
			center: { x: spec.position.x, y:spec.position.y },
			speed: spec.speed,
			lifetime: spec.lifetime,
		});
	};
	
	//------------------------------------------------------------------
	//
	// Update the state of all particles.  This includes remove any that 
	// have exceeded their lifetime.
	//
	//------------------------------------------------------------------
	that.update = function(elapsedTime) {
		var removeMe = [],
			value,
			effect,
			particle;
			
		//
		// We work with time in seconds, elapsedTime comes in as milliseconds
		elapsedTime = elapsedTime / 1000;

		for (effect in effects) {
			if (effects[effect].update(elapsedTime)) {
				removeMe.push(effect);
			}
		}

		//
		// Remove all of the expired effects
		for (effect = 0; effect < removeMe.length; effect++) {
			delete effects[removeMe[effect]];
		}
		removeMe.length = 0;
		
		for (value in particles) {
			if (particles.hasOwnProperty(value)) {
				particle = particles[value];
				//
				// Update how long it has been alive
				particle.alive += elapsedTime;
				
				//
				// Update its position
				particle.center.x += (elapsedTime * particle.speed * particle.direction.x);
				particle.center.y += (elapsedTime * particle.speed * particle.direction.y);
				
				//
				// Rotate proportional to its speed
				particle.rotation += particle.speed / 500;
				
				//
				// If the lifetime has expired, identify it for removal
				if (particle.alive > particle.lifetime) {
					removeMe.push(value);
				}
			}
		}

		//
		// Remove all of the expired particles
		for (particle = 0; particle < removeMe.length; particle++) {
			delete particles[removeMe[particle]];
		}
		removeMe.length = 0;
	};
	
	//------------------------------------------------------------------
	//
	// When a particle system is first created, this function is empty.
	// Once the texture for the particle system is loaded, this function
	// gets replaced with one that will actually render things.
	//
	//------------------------------------------------------------------
	that.render = function() {
		that.render = function() {
			var value,
				particle;
			
			for (value in particles) {
				if (particles.hasOwnProperty(value)) {
					particle = particles[value];
					Graphics.drawParticle(particle);
				}
			}
		};
	};

    // Allow for changing the center of the particle system
    that.changeCenter = function(newCenter) {
        spec.center.x = newCenter.x;
        spec.center.y = newCenter.y;
    };

	that.initialize = function() {
		console.log('Initializing particle system.')
	}
	
	return that;

})(Graphics);