'use strict';

let NUM_PARTICLES = 50;
let gameOverRotation = 0;
let gameOverRotationChange = Math.PI/4;
let countdownTime = 0;
let spriteTime = 0;
let spriteLocation = { x: 0, y: 400 };

let Renderer = (function() {
    let particleSystems = [];
    let myGameOverText = null;
    let weakEnemy = null;
    let strongEnemy = null;
    let flyingEnemy = null;
    let towerBase = null;
    let gameboard = null;

    function drawBackground() {
        let myBackground = Graphics.Rectangle({
            corner: { x: 0, y: 0 },
            size: { w: Graphics.getCanvasWidth(), h: Graphics.getCanvasHeight() },
            rotation: 0,
            fillStyle: 'rgba(0, 200, 0, 1)',
            strokeStyle: 'rgba(0, 0, 0, 0)'
          });
          myBackground.draw();
    }

    function drawCountdown() {
        var myText = Graphics.Text({
            text: Math.floor(countdownTime/1000+1),
            font: '100px Arial, sans-serif',
            fill: 'rgba(255,0,0,1)',
            stroke: 'rgba(0,0,0,1)',
            location: {x: Graphics.getCanvasWidth()/2, y: Graphics.getCanvasHeight()/2 },
            rotation: 0
        });

        myText.draw();
    }

    function drawGameOver() {
        var myText = null;
        if (gameboard.isWin()) {
            myText = Graphics.Text({
                text: 'You Win!',
                font: '100px Arial, sans-serif',
                fill: 'rgba(255,0,0,1)',
                stroke: 'rgba(0,0,0,1)',
                location: {x: Graphics.getCanvasWidth()/2, y: Graphics.getCanvasHeight()/2 - 50 },
                rotation: gameOverRotation
            });
        } else {
            myText = Graphics.Text({
                text: 'Game Over!',
                font: '100px Arial, sans-serif',
                fill: 'rgba(255,0,0,1)',
                stroke: 'rgba(0,0,0,1)',
                location: {x: Graphics.getCanvasWidth()/2, y: Graphics.getCanvasHeight()/2 - 50 },
                rotation: gameOverRotation
            });
        }

        myText.draw();
        myText = Graphics.Text({
            text: 'Press esc to return to main menu!',
            font: '25px Arial, sans-serif',
            fill: 'rgba(255,0,0,1)',
            stroke: 'rgba(0,0,0,1)',
            location: {x: Graphics.getCanvasWidth()/2, y: Graphics.getCanvasHeight()/2 + 50 },
            rotation: gameOverRotation
        });

        myText.draw();
    }

    function drawCells() { 
        let myBoard = Graphics.Rectangle({
            corner: { x: gameboard.playAreaXOffset, y: gameboard.playAreaYOffset },
            size: { w: gameboard.numCellsWide*gameboard.cellWidth, 
                h: gameboard.numCellsHigh*gameboard.cellHeight },
                rotation: 0,
                fillStyle: 'rgba(250, 250, 250, 0)',
                strokeStyle: 'rgba(0, 0, 0, 1)',
        });
        myBoard.draw();

        // Draw corners
        let myWall = Graphics.Rectangle({
            corner: { x: gameboard.playAreaXOffset-20, y: gameboard.playAreaYOffset-20 },
            size: { w: (gameboard.numCellsWide-2)/2*gameboard.cellWidth, 
                h: 20 },
                rotation: 0,
                fillStyle: 'rgba(250, 250, 250, 1)',
                strokeStyle: 'rgba(250, 250, 250, 1)',
        });
        myWall.draw();
        myWall = Graphics.Rectangle({
            corner: { x: gameboard.playAreaXOffset-20, y: gameboard.playAreaYOffset-20 },
            size: { w: 20, 
                h: (gameboard.numCellsHigh-2)/2*gameboard.cellHeight },
                rotation: 0,
                fillStyle: 'rgba(250, 250, 250, 1)',
                strokeStyle: 'rgba(250, 250, 250, 1)',
        });
        myWall.draw();
        myWall = Graphics.Rectangle({
            corner: { x: gameboard.playAreaXOffset + (gameboard.numCellsWide+2)*gameboard.cellWidth/2 + 20,
                 y: gameboard.playAreaYOffset-20 },
            size: { w: (gameboard.numCellsWide-2)/2*gameboard.cellWidth, 
                h: 20 },
                rotation: 0,
                fillStyle: 'rgba(250, 250, 250, 1)',
                strokeStyle: 'rgba(250, 250, 250, 1)',
        });
        myWall.draw();
        myWall = Graphics.Rectangle({
            corner: { x: gameboard.playAreaXOffset + gameboard.numCellsWide*gameboard.cellWidth,
                 y: gameboard.playAreaYOffset-20 },
            size: { w: 20, 
                h: (gameboard.numCellsHigh-2)/2*gameboard.cellHeight },
                rotation: 0,
                fillStyle: 'rgba(250, 250, 250, 1)',
                strokeStyle: 'rgba(250, 250, 250, 1)',
        });
        myWall.draw();
        myWall = Graphics.Rectangle({
            corner: { x: gameboard.playAreaXOffset + (gameboard.numCellsWide+2)*gameboard.cellWidth/2 +20,
                 y: gameboard.playAreaYOffset + gameboard.numCellsHigh*gameboard.cellHeight},
            size: { w: (gameboard.numCellsWide-2)/2*gameboard.cellWidth, 
                h: 20 },
                rotation: 0,
                fillStyle: 'rgba(250, 250, 250, 1)',
                strokeStyle: 'rgba(250, 250, 250, 1)',
        });
        myWall.draw();
        myWall = Graphics.Rectangle({
            corner: { x: gameboard.playAreaXOffset + gameboard.numCellsWide*gameboard.cellWidth,
                 y: gameboard.playAreaYOffset + (gameboard.numCellsHigh+2)*gameboard.cellHeight/2 + 20 },
            size: { w: 20, 
                h: (gameboard.numCellsHigh-2)/2*gameboard.cellHeight },
                rotation: 0,
                fillStyle: 'rgba(250, 250, 250, 1)',
                strokeStyle: 'rgba(250, 250, 250, 1)',
        });
        myWall.draw();
        myWall = Graphics.Rectangle({
            corner: { x: gameboard.playAreaXOffset-20,
                 y: gameboard.playAreaYOffset + gameboard.numCellsHigh*gameboard.cellHeight},
            size: { w: (gameboard.numCellsWide-2)/2*gameboard.cellWidth, 
                h: 20 },
                rotation: 0,
                fillStyle: 'rgba(250, 250, 250, 1)',
                strokeStyle: 'rgba(250, 250, 250, 1)',
        });
        myWall.draw();
        myWall = Graphics.Rectangle({
            corner: { x: gameboard.playAreaXOffset-20,
                 y: gameboard.playAreaYOffset + (gameboard.numCellsHigh+2)*gameboard.cellHeight/2 + 20 },
            size: { w: 20, 
                h: (gameboard.numCellsHigh-2)/2*gameboard.cellHeight },
                rotation: 0,
                fillStyle: 'rgba(250, 250, 250, 1)',
                strokeStyle: 'rgba(250, 250, 250, 1)',
        });
        myWall.draw();

        /*
        for (let x = 0; x < gameboard.cells.length; x++) {
            for (let y = 0; y < gameboard.cells[x].length; y++) {
                let myCell = Graphics.Rectangle({
                    corner: { x: gameboard.playAreaXOffset+gameboard.cells[x][y].x*gameboard.cellWidth,
                         y: gameboard.playAreaYOffset+gameboard.cells[x][y].y*gameboard.cellHeight },
                    size: { w: gameboard.cellWidth, h: gameboard.cellHeight },
                    rotation: 0,
                    fillStyle: 'rgba(0, 200, 0, 1)',
                    strokeStyle: 'rgba(0, 0, 0, 1)'
                });
                myCell.draw();
            }
        } 
        */
    }

    function drawButton(button) {
        let buttonBackground = Graphics.Rectangle({
                corner: { x: button.x, y: button.y },
                size: { w: button.w, h: button.h },
                rotation: 0,
                fillStyle: 'rgba(200, 200, 200, 1)',
                strokeStyle: 'rgba(0, 0, 0, 1)'
        });
        buttonBackground.draw();
        
        if (button.bkgdImage !== null) {
            // Tower base image
            let bkgdImage = Graphics.Texture({
                image: button.bkgdImage,
                center: { x: button.x+button.w/2,
                    y: button.y+button.h/2 },
                width: button.w,
                height: button.h,
                rotation: 0
            });
            bkgdImage.draw();
        }
        if (button.image !== null) {
            if (typeof button.spriteDimensions == 'undefined') {
                let image = Graphics.Texture({
                    image: button.image,
                    center: { x: button.x+button.w/2,
                        y: button.y+button.h/2 },
                    width: button.w,
                    height: button.h,
                    rotation: 0
                });
                image.draw();
            } else {
                let rotation = Math.PI;
                if (button.image == 'iceTower') {
                    rotation = Math.PI/2;
                }
                let image = Graphics.Sprite({
                    image: button.image,
                    center: { x: button.x+button.w/2,
                        y: button.y+button.h/2 },
                    width: button.w,
                    height: button.h,
                    rotation: rotation,
                    spriteDimensions: button.spriteDimensions,
                });
                image.draw(0);
            }
        }
        if (button.text !== null) {
            var myText = Graphics.Text({
                text: button.text,
                font: '10pt Arial, sans-serif',
                fill: 'rgba(25,25,25,1)',
                stroke: 'rgba(0,0,0,1)',
                location: {x: button.x + button.w/2, y: button.y + button.h/2 },
                rotation: 0,
            });

            myText.draw();
        }
        if (button.selected) {
            let border = Graphics.Rectangle({
                corner: { x: button.x, y: button.y },
                size: { w: button.w, h: button.h },
                rotation: 0,
                fillStyle: 'rgba(255, 255, 0, .25)',
                strokeStyle: 'rgba(255, 255, 0, 1)'
            });
            border.draw();
        }
    }

    function drawTower(tower) {
        let border = Graphics.Rectangle({
            corner: { x: tower.x-(tower.getDrawWidth()/2), y: tower.y-(tower.getDrawHeight()/2) },
            size: { w: tower.getDrawWidth(), h: tower.getDrawHeight() },
            rotation: 0,
            fillStyle: 'rgba(200, 200, 200, .5)',
            strokeStyle: 'rgba(0, 0, 0, 1)'
        });
        border.draw();
        towerBase.changeCenter({x: tower.x, y: tower.y});
        towerBase.changeSize(tower.getDrawWidth(), tower.getDrawHeight());
        towerBase.draw();
        let rotation = Math.PI;
        if (tower.image == 'iceTower') {
            rotation = Math.PI/2;
        }
        // Tower Top image
        let towerTop = Graphics.Sprite({
            image: tower.image,
            center: { x: tower.x,
                 y: tower.y },
            width: tower.getDrawWidth(),
            height: tower.getDrawHeight(),
            rotation: tower.rotation + rotation, // Images are flipped 180 degrees right now.
            spriteDimensions: { x: 1000, y: 1000 },
        });
        towerTop.draw(tower.level-1);
        if (tower.upgrading) {
            drawUpgradeBar(tower);
        }
    }

    function drawUpgradeBar(tower) {
        let maxUpgradeBar = Graphics.Rectangle({
            corner: { x: tower.x - 15, y: tower.y+10 },
            size: { w: 30, h: 5 },
            rotation: 0,
            fillStyle: 'rgba(102, 102, 0, 1)',
            strokeStyle: 'rgba(0, 0, 0, 1)'
        });
        maxUpgradeBar.draw();
        let upgradeBar = Graphics.Rectangle({
            corner: { x: tower.x - 15, y: tower.y+10 },
            size: { w: 30*(tower.timeUpgrading/tower.upgradeTime), h: 5 },
            rotation: 0,
            fillStyle: 'rgba(255, 255, 0, 1)',
            strokeStyle: 'rgba(0, 0, 0, 1)'
        });
        upgradeBar.draw();
    }

    function drawProjectile(projectile) {
        let myProjectile = Graphics.Texture({
            image: projectile.image,
            center: { x: projectile.x, y: projectile.y },
            width: projectile.width,
            height: projectile.height,
            rotation: projectile.rotation
        });
        myProjectile.draw();
    }

    function drawToPlaceTower(tower) {
        drawTower(tower);
        if (tower.placeable === true) {
            let myPlaceable = Graphics.Rectangle({
                corner: { x: tower.x-(tower.getDrawWidth()/2), y: tower.y-(tower.getDrawHeight()/2) },
                size: { w: tower.getDrawWidth(), h: tower.getDrawHeight() },
                rotation: 0,
                fillStyle: 'rgba(0, 150, 0, .5)',
                strokeStyle: 'rgba(0, 0, 0, 0)'
            });
            myPlaceable.draw();
        } else {
            let myPlaceable = Graphics.Rectangle({
                corner: { x: tower.x-(tower.getDrawWidth()/2), y: tower.y-(tower.getDrawHeight()/2) },
                size: { w: tower.getDrawWidth(), h: tower.getDrawHeight() },
                rotation: 0,
                fillStyle: 'rgba(150, 0, 0, .5)',
                strokeStyle: 'rgba(0, 0, 0, 0)'
            });
            myPlaceable.draw();
        }
        let rangeCircle = Graphics.Circle({
            center: { x: tower.x, y: tower.y },
            radius: tower.range,
            fillStyle: 'rgba(150, 150, 150, .5)',
            strokeStyle: 'rgba(0, 0, 0, 0)'
        });
        rangeCircle.draw();
    }

    function drawWeakEnemy(creep) {
        // Loop through weak enemies and have them draw themselves.
        weakEnemy.changeCenter({x: creep.x, y: creep.y});
        weakEnemy.changeRotation(creep.rotation);
        weakEnemy.draw(creep.animationFrame);
    }

    function drawStrongEnemy(creep) {
        // Loop through strong enemies and have them draw themselves.
        strongEnemy.changeCenter({x: creep.x, y: creep.y});
        strongEnemy.changeRotation(creep.rotation);
        strongEnemy.draw(creep.animationFrame);
    }

    function drawFlyingEnemy(creep) {
        flyingEnemy.changeCenter({x: creep.x, y: creep.y});
        strongEnemy.changeRotation(creep.rotation);
        flyingEnemy.draw(creep.animationFrame);
    }

    function drawHealthBar(creep) {
        let maxHealthBar = Graphics.Rectangle({
            corner: { x: creep.x - 20, y: creep.y-15 },
            size: { w: 40, h: 5 },
            rotation: 0,
            fillStyle: 'rgba(255, 0, 0, 1)',
            strokeStyle: 'rgba(0, 0, 0, 1)'
        });
        maxHealthBar.draw();
        let healthBar = Graphics.Rectangle({
            corner: { x: creep.x - 20, y: creep.y-15 },
            size: { w: 40*(creep.health/creep.maxHealth), h: 5 },
            rotation: 0,
            fillStyle: 'rgba(0, 255, 0, 1)',
            strokeStyle: 'rgba(0, 0, 0, 1)'
        });
        healthBar.draw();
    }

    function drawSelectedTowerIndicators() {
        let tower = gameboard.selectedTower;

        if (tower !== null) {

            let border = Graphics.Rectangle({
                corner: { x: tower.x-(tower.getDrawWidth()/2), y: tower.y-(tower.getDrawHeight()/2) },
                size: { w: tower.getDrawWidth(), h: tower.getDrawHeight() },
                rotation: 0,
                fillStyle: 'rgba(255, 255, 0, .25)',
                strokeStyle: 'rgba(255, 255, 0, 1)'
            });
            border.draw();

            let fireArc = Graphics.Arc({
                center: { x: tower.x, y: tower.y },
                radius: tower.range,
                rotation: tower.rotation,
                angle: tower.fireArc,
                fillStyle: 'rgba(150, 0, 0, .5)',
                strokeStyle: 'rgba(0, 0, 0, 0)'
            });
            fireArc.draw();
        }
    }

    function drawSelectedInformation() {
        let tower = gameboard.selectedTower;
    
        let xOffset = 50 + gameboard.playAreaXOffset + gameboard.numCellsWide*gameboard.cellWidth;
        let yOffset = 75 + gameboard.playAreaYOffset;
        let infoBoxW = 275;
        let infoBoxH = 450;
        let textXOffset = xOffset + infoBoxW/2;
        let textFillStyle = 'rgba(25,25,25,1)';
        let textStrokeStyle = 'rgba(0, 0, 0,1)';

        let background = Graphics.Rectangle({
                corner: { x: xOffset, y: yOffset },
                size: { w: infoBoxW, h: infoBoxH },
                rotation: 0,
                fillStyle: 'rgba(100, 100, 100, 1)',
                strokeStyle: 'rgba(20, 20, 20, 1)'
            })
        background.draw();

        if (tower !== null) {
            myText = Graphics.Text({
                    text: 'Value: ' + tower.cost,
                    font: '18pt Arial, sans-serif',
                    fill: textFillStyle,
                    stroke: textStrokeStyle,
                    location: {x: textXOffset, y: yOffset + 25 },
                    rotation: 0,
                });
            myText.draw();
            var myText = Graphics.Text({
                    text: 'Damage: ' + tower.damage,
                    font: '18pt Arial, sans-serif',
                    fill: textFillStyle,
                    stroke: textStrokeStyle,
                    location: {x: textXOffset, y: yOffset + 75 },
                    rotation: 0,
                });
            myText.draw();
            myText = Graphics.Text({
                    text: 'Range: ' + tower.range,
                    font: '18pt Arial, sans-serif',
                    fill: textFillStyle,
                    stroke: textStrokeStyle,
                    location: {x: textXOffset, y: yOffset + 125 },
                    rotation: 0,
                });
            myText.draw();
            myText = Graphics.Text({
                    text: 'Fire rate: ' + tower.fireRate + ' shots/sec',
                    font: '18pt Arial, sans-serif',
                    fill: textFillStyle,
                    stroke: textStrokeStyle,
                    location: {x: textXOffset, y: yOffset + 175 },
                    rotation: 0,
                });
            myText.draw();
            myText = Graphics.Text({
                    text: 'AOE: ' + tower.damageRadius,
                    font: '18pt Arial, sans-serif',
                    fill: textFillStyle,
                    stroke: textStrokeStyle,
                    location: {x: textXOffset, y: yOffset + 225 },
                    rotation: 0,
                });
            myText.draw();
            myText = Graphics.Text({
                    text: 'Level: ' + tower.level + '/' + tower.maxLevel,
                    font: '18pt Arial, sans-serif',
                    fill: textFillStyle,
                    stroke: textStrokeStyle,
                    location: {x: textXOffset, y: yOffset + 275 },
                    rotation: 0,
                });
            myText.draw();
            myText = Graphics.Text({
                    text: 'Targets: ' + tower.targetTypes,
                    font: '18pt Arial, sans-serif',
                    fill: textFillStyle,
                    stroke: textStrokeStyle,
                    location: {x: textXOffset, y: yOffset + 325 },
                    rotation: 0,
                });
            myText.draw();
            myText = Graphics.Text({
                    text: 'Upgrade Cost: ' + tower.upgradeCost,
                    font: '18pt Arial, sans-serif',
                    fill: textFillStyle,
                    stroke: textStrokeStyle,
                    location: {x: textXOffset, y: yOffset + 375 },
                    rotation: 0,
                });
            myText.draw();
            myText = Graphics.Text({
                    text: 'Sell Value: ' + tower.cost*.50,
                    font: '18pt Arial, sans-serif',
                    fill: textFillStyle,
                    stroke: textStrokeStyle,
                    location: {x: textXOffset, y: yOffset + 425 },
                    rotation: 0,
                });
            myText.draw();
        }
    }

    function drawGameInfo() {
        let myText = Graphics.Text({
                    text: 'Lives: ' + gameboard.lives,
                    font: '18pt Arial, sans-serif',
                    fill: 'rgba(255,0,0,1)',
                    stroke: 'rgba(255,0,0,1)',
                    location: {x: 500, y: 25 },
                    rotation: 0,
                });
        myText.draw();
        myText = Graphics.Text({
                    text: 'Money: ' + gameboard.money,
                    font: '18pt Arial, sans-serif',
                    fill: 'rgba(255,255,0,1)',
                    stroke: 'rgba(255,255,0,1)',
                    location: {x: 700, y: 25 },
                    rotation: 0,
                });
        myText.draw();
        myText = Graphics.Text({
                    text: 'Score: ' + gameboard.score,
                    font: '18pt Arial, sans-serif',
                    fill: 'rgba(25,25,25,1)',
                    stroke: 'rgba(0,0,0,1)',
                    location: {x: 900, y: 25 },
                    rotation: 0,
                });
        myText.draw();
    }

    function drawGameboard() {
        drawCells();

        for (let i = 0; i < gameboard.buttons.length; i++) {
            drawButton(gameboard.buttons[i]);
        }

        for (let tower in gameboard.towers) {
            drawTower(gameboard.towers[tower]);
        }

        for (let creep in gameboard.creeps) {
            if (gameboard.creeps[creep].type == 'weak') {
                drawWeakEnemy(gameboard.creeps[creep]);
            } else if (gameboard.creeps[creep].type == 'strong') {
                drawStrongEnemy(gameboard.creeps[creep]);
            } else {
                drawFlyingEnemy(gameboard.creeps[creep]);
            }
            drawHealthBar(gameboard.creeps[creep]);
        }

        for (let tower in gameboard.towers) {
            let projectile = 0;
            for (projectile in gameboard.towers[tower].projectiles) {
                drawProjectile(gameboard.towers[tower].projectiles[projectile]);
            }
        }
        drawSelectedTowerIndicators();
        drawSelectedInformation();
        drawGameInfo();
    }

    function initializeImages() {
        console.log('Initializing images');

        // Weak Enemy Sprite
        weakEnemy = Graphics.Sprite({
            image: 'moleSprite',
            center: { x: 200,
                 y: 100 },
            width: 50,
            height: 50*(250/500),
            rotation: 0,
            spriteDimensions: { x: 500, y: 250 }
        });
        
        // Strong Enemy Sprite
        strongEnemy = Graphics.Sprite({
            image: 'strongMoleSprite',
            center: { x: 200,
                 y: 100 },
            width: 50,
            height: 50*(250/500),
            rotation: 0,
            spriteDimensions: { x: 500, y: 250 }
        });

        // Flying Enemy Sprite
        flyingEnemy = Graphics.Sprite({
            image: 'flyingMoleSprite',
            center: { x: 200,
                 y: 200 },
            width: 50,
            height: 50*(450/500),
            rotation: 0,
            spriteDimensions: { x: 500, y: 450 }
        });

        // Tower base image
        towerBase = Graphics.Texture({
            image: 'towerBase',
            center: { x: -100,
                 y: 0 },
            width: gameboard.cellWidth,
            height: gameboard.cellHeight,
            rotation: 0
        });
    }

    function update(elapsedTime) {
        
        gameOverRotation += gameOverRotationChange * (elapsedTime/1000);
        // Have the game over screen rotate between two points
        if (gameOverRotation < -Math.PI/6) {
            gameOverRotationChange = -gameOverRotationChange;
            gameOverRotation = -Math.PI/6;
        }
        else if (gameOverRotation > Math.PI/6) {
            gameOverRotationChange = -gameOverRotationChange;
            gameOverRotation = Math.PI/6;
        }

        // Update the particle system
        ParticleSystem.update(elapsedTime);
    };

    function render(elapsedTime) {
        if (gameboard !== null) {
            Graphics.beginRender();
            drawBackground();
            drawGameboard();
            for (var system in particleSystems) {
                if (particleSystems.hasOwnProperty(system)) {
                    particleSystems[system].render(elapsedTime);
                }
            }
            // Handle tower placement options
            if (gameboard.placeTower && gameboard.towerToPlace !== null) {
                drawToPlaceTower(gameboard.towerToPlace);
            }

            // Draw particle effects
            ParticleSystem.render(elapsedTime);

            if (gameboard.gameOver) {
                drawGameOver();
            }
        }
    };

    function initialize(playArea) {
        console.log('Initializing Renderer');

        // Setup the graphics layer
        Graphics.initialize();

        gameboard = playArea;

        initializeImages();
    }

    return {
        update: update,
        render: render,
        initialize: initialize
    }
}());


