MyGame.screens['credits'] = (function(game) {
    'use strict';

    function run() {
        // Intentionally blank
    }

    function initialize() {
		document.getElementById('credits-back-btn').addEventListener(
			'click',
            function() {
                game.showScreen('mainMenu'); });
    }

    return {
        initialize: initialize,
        run: run
    };
}(MyGame.main));