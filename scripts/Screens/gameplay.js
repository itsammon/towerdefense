'use strict';

MyGame.screens['gameplay'] = (function(input) {
    let previousTime = performance.now();
    let elapsedTime = 0;
    let myKeyboard = null;
    let myMouse = null;
    let playArea = null;
    let paused = true;

    function toMenu() {
        paused = true;

        MyGame.main.showScreen('mainMenu');
    }

    function nextLevel() {
        playArea.nextLevel();
    }

    function upgrade() {
        playArea.upgrade();
    }

    function sellTower() {
        playArea.sellTower();
    }

    function getMousePos(event) {
        let rect = Graphics.getCanvasBoundingRect();
        return {
            x: Math.floor((event.clientX-rect.left)/(rect.right-rect.left)*Graphics.getCanvasWidth()), 
            y: Math.floor((event.clientY-rect.top)/(rect.bottom-rect.top)*Graphics.getCanvasHeight())
        }
    }

    function mouseDown(elapsedTime, event) {
        playArea.mouseDown(getMousePos(event));
    }

    function mouseMove(elapsedTime, event) {
        playArea.mouseOver(getMousePos(event));
    }

    function handleInput(elapsedTime) {
        myKeyboard.update(elapsedTime);
        myMouse.update(elapsedTime);
    }

    function update(elapsedTime) {
        playArea.update(elapsedTime);
        Renderer.update(elapsedTime);
    }

    function render(elapsedTime) {
        Renderer.render(elapsedTime);
    }

    function gameLoop(time) {
        elapsedTime = time - previousTime;
        previousTime = time;

        if (!paused) {
            handleInput(elapsedTime);
            update(elapsedTime);
            render(elapsedTime);
        }
        
        requestAnimationFrame(gameLoop);
    }

    function setInput() {
        // Get the keyboard input
        myKeyboard = input.Keyboard();

        // Register keyboard commands
        myKeyboard.registerCommand(KeyEvent.DOM_VK_ESCAPE, toMenu);

        // Get input settings from the persistent storage
        var keybindings = MyGame.persistence.getControls();

        if (keybindings.hasOwnProperty('nextLevel')) {
            myKeyboard.registerCommand(keybindings['nextLevel'], nextLevel);
        } else {
            myKeyboard.registerCommand(KeyEvent.DOM_VK_G, nextLevel);
        }
        if (keybindings.hasOwnProperty('upgrade')) {
            myKeyboard.registerCommand(keybindings['upgrade'], upgrade);
        } else {
            myKeyboard.registerCommand(KeyEvent.DOM_VK_U, upgrade);
        }
        if (keybindings.hasOwnProperty('sellTower')) {
            myKeyboard.registerCommand(keybindings['sellTower'], sellTower);
        } else {
            myKeyboard.registerCommand(KeyEvent.DOM_VK_S, sellTower);
        }

        // Get the mouse input
        myMouse = input.Mouse();
        
        // Register mouse commands
        myMouse.registerCommand('mousedown', mouseDown);
        myMouse.registerCommand('mousemove', mouseMove);
    }

    function newGame() {
        playArea.newGame();
    }

    function run() {
        setInput();

        paused = false;
    }

    function initialize() {
        console.log('Initializing Gameplay Screen');

        playArea = PlayArea();
        playArea.initialize();

        // Initialize graphics system
        Renderer.initialize(playArea);

        requestAnimationFrame(gameLoop);
    }

    return {
        newGame: newGame,
        initialize: initialize,
        run: run
    };
}(MyGame.input));