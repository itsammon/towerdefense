MyGame.screens['highscores'] = (function(game) {
    'use strict';

    let myKeyboard = null;

    function run() {
        // Need to get the high scores into the screens
        MyGame.highscores.showHighScores();
    }

    function initialize() {
		document.getElementById('h-back-btn').addEventListener(
			'click',
            function() {
                game.showScreen('mainMenu'); });
    }

    return {
        initialize: initialize,
        run: run
    };
}(MyGame.main));