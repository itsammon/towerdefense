MyGame.screens['controls'] = (function(game) {
    'use strict';

    var buttonsExist = false;
    var input = null;
    var currControl = null;

    function chooseControl(control) {
        currControl = control;
        unselectControls();

        document.getElementById(control).classList.add('aControl');
        window.addEventListener('keydown', setControl);
    }

    function setControl(event) {
        unselectControls();

        MyGame.persistence.setControl(currControl, event.keyCode);
        refreshKeybindings();
    }

    function unselectControls() {
        let active = null;

        active = document.getElementsByClassName('aControl');
        for (input = 0; input < active.length; input++) {
            active[input].classList.remove('aControl');
            window.removeEventListener('keydown', setControl);
        }
    }

    // Not currently used
    function removeEventListeners() {
        document.getElementById('nextLevel-btn').removeEventListener('click');
        document.getElementById('upgrade-btn').removeEventListener('click');
        document.getElementById('sellTower-btn').removeEventListener('click');
    }

    function addEventListeners() {
		document.getElementById('nextLevel-btn').addEventListener(
			'click',
            function() {
                chooseControl('nextLevel');
            });

		document.getElementById('upgrade-btn').addEventListener(
			'click',
            function() {
                chooseControl('upgrade');
            });

		document.getElementById('sellTower-btn').addEventListener(
			'click',
            function() {
                chooseControl('sellTower');
            });
    }

    function refreshKeybindings() {
        var keybindingsDisp = document.getElementById('keybindingsDiv');
        var keybindings = MyGame.persistence.getControls();
        var textField = '';

        keybindingsDisp.innerHTML = '<button class="keybindingBtn" id="nextLevel-btn">Next Level</button>';
        textField = '<input id="nextLevel" class="keybinding" type="text" readonly value="';
        if (keybindings.hasOwnProperty('nextLevel')) {
            textField += String.fromCharCode(keybindings['nextLevel']);
        } else {
            textField += String.fromCharCode(KeyEvent['DOM_VK_G']);
        }
        textField += '" /><br />';
        keybindingsDisp.innerHTML += textField;

        keybindingsDisp.innerHTML += '<button class="keybindingBtn" id="upgrade-btn">Upgrade Tower</button>';
        textField = '<input id="upgrade" class="keybinding" type="text" readonly value="';
        if (keybindings.hasOwnProperty('upgrade')) {
            textField += String.fromCharCode(keybindings['upgrade']);
        } else {
            textField += String.fromCharCode(KeyEvent['DOM_VK_U']);
        }
        textField += '" /><br />';
        keybindingsDisp.innerHTML += textField;

        keybindingsDisp.innerHTML += '<button class="keybindingBtn" id="sellTower-btn">Sell Tower</button>';
        textField = '<input id="sellTower" class="keybinding" type="text" readonly value="';
        if (keybindings.hasOwnProperty('sellTower')) {
            textField += String.fromCharCode(keybindings['sellTower']);
        } else {
            textField += String.fromCharCode(KeyEvent['DOM_VK_S']);
        }
        textField += '" /><br />';
        keybindingsDisp.innerHTML += textField;

        addEventListeners();
    }

    function run() {
        refreshKeybindings();
    }

    function initialize() {
        input = MyGame.input.Keyboard();
		
		document.getElementById('c-reset-btn').addEventListener(
			'click',
            function() {
                unselectControls();

                MyGame.persistence.resetControls();
                refreshKeybindings();
             });
		
		document.getElementById('c-back-btn').addEventListener(
			'click',
            function() {
                unselectControls();

                game.showScreen('mainMenu'); });
    }

    return {
        initialize: initialize,
        run: run
    };
}(MyGame.main));