/* globals Demo, console, require */

MyGame = {
	input: {},
	components: {},
	renderer: {},
	assets: {},
    screens: {},
    cancelNextFrame: false,

};


//------------------------------------------------------------------
//
// Purpose of this code is to bootstrap (maybe I should use that as the name)
// the rest of the application.  Only this file is specified in the index.html
// file, then the code in this file gets all the other code and assets
// loaded.
//
//------------------------------------------------------------------
MyGame.loader = (function() {
	'use strict';
	var scriptOrder = [{
			scripts: ['input'],
			message: 'Inputs loaded',
			onComplete: null
		}, {
			scripts: ['Components/playArea'],
			message: 'Components loaded',
			onComplete: null
		}, {
			scripts: ['audio'],
			message: 'Audio loaded',
			onComplete: null
		}, {
			scripts: ['Graphics/graphics'],
			message: 'Graphics core loaded',
			onComplete: null
		}, {
			scripts: ['Graphics/random'],
			message: 'Graphics random loaded',
			onComplete: null
		}, {
			scripts: ['Graphics/particleSystem'],
			message: 'Graphics particle system loaded',
			onComplete: null
		}, {
			scripts: ['Graphics/renderer'],
			message: 'Rendering loaded',
			onComplete: null
		}, {
			scripts: ['game'],
			message: 'Game loaded',
			onComplete: null
		}, {
			scripts: ['Screens/credits'],
			message: 'Credits loaded',
			onComplete: null
		}, {
			scripts: ['Screens/gameplay'],
			message: 'Gameplay screen loaded',
			onComplete: null
		}, {
			scripts: ['Screens/controls'],
			message: 'Controls loaded',
			onComplete: null
		}, {
			scripts: ['Screens/highscores'],
			message: 'High scores menu loaded',
			onComplete: null
		}, {
			scripts: ['Screens/mainMenu'],
			message: 'Main menu loaded',
			onComplete: null
		}],
		assetOrder = [{
			key: 'fire',
			source: '/images/fire.png'
		}, {
			key: 'smoke',
			source: '/images/smoke.png'
		}, {
			key: 'flyingMoleSprite',
			source: '/images/flyingMoleSprite.png'
		}, {
			key: 'moleSprite',
			source: '/images/moleSprite2.0.png'
		}, {
			key: 'strongMoleSprite',
			source: '/images/strongMoleSprite.png'
		}, {
			key: 'testTowerTop',
			source: '/images/tempImages/turret-1-1.png'
		}, {
			key: 'towerBase',
			source: '/images/towerBase.png'
		}, {
			key: 'projectileTower',
			source: '/images/tower1Top.png'
		}, {
			key: 'bombTower',
			source: '/images/towerBombTop.png'
		}, {
			key: 'missileTower',
			source: '/images/towerMissileTop.png'
		}, {
			key: 'iceTower',
			source: '/images/towerIceTop.png'
		}, {
			key: 'bullet',
			source: '/images/bulletSmall.png'
		}, {
			key: 'bomb',
			source: '/images/Bomb.png'
		}, {
			key: 'missile',
			source: '/images/missile.png'
		}, {
			key: 'ice',
			source: '/images/iceBullet.png'
		}, {
			key: 'backgroundMusic.mp3',
			source: '/audio/Level_Up.mp3'
		}, {
			key: 'creepDeath.mp3',
			source: '/audio/Jab.mp3'
		}, {
			key: 'basicShot.mp3',
			source: '/audio/tempAudio/sound-2.mp3'
		}, {
			key: 'damageSound.mp3',
			source: '/audio/tempAudio/sound-2.mp3'
		}, {
			key: 'iceSound.mp3',
			source: '/audio/Debris_Hits.mp3'
		}, {
			key: 'shot.mp3',
			source: '/audio/Dirt_Shovel_On_Coffin.mp3'
		}, {
			key: 'bombExplosion.mp3',
			source: '/audio/Glass_Windows_Crashing.mp3'
		}, {
			key: 'missileExplosion.mp3',
			source: '/audio/Rubble_Crash.mp3'
		}, {
			key: 'bulletSound.mp3',
			source: '/audio/Windshield_Hit_With_Bar.mp3'
		}];

	//------------------------------------------------------------------
	//
	// Helper function used to load scripts in the order specified by the
	// 'scripts' parameter.  'scripts' expects an array of objects with
	// the following format...
	//	{
	//		scripts: [script1, script2, ...],
	//		message: 'Console message displayed after loading is complete',
	//		onComplete: function to call when loading is complete, may be null
	//	}
	//
	//------------------------------------------------------------------
	function loadScripts(scripts, onComplete) {
		var entry = 0;
		//
		// When we run out of things to load, that is when we call onComplete.
		if (scripts.length > 0) {
			entry = scripts[0];
			require(entry.scripts, function() {
				console.log(entry.message);
				if (entry.onComplete) {
					entry.onComplete();
				}
				scripts.splice(0, 1);
				loadScripts(scripts, onComplete);
			});
		} else {
			onComplete();
		}
	}

	//------------------------------------------------------------------
	//
	// Helper function used to load assets in the order specified by the
	// 'assets' parameter.  'assets' expects an array of objects with
	// the following format...
	//	{
	//		key: 'asset-1',
	//		source: 'asset/url/asset.png'
	//	}
	//
	// onSuccess is invoked per asset as: onSuccess(key, asset)
	// onError is invoked per asset as: onError(error)
	// onComplete is invoked once per 'assets' array as: onComplete()
	//
	//------------------------------------------------------------------
	function loadAssets(assets, onSuccess, onError, onComplete) {
		var entry = 0;
		//
		// When we run out of things to load, that is when we call onComplete.
		if (assets.length > 0) {
			entry = assets[0];
			loadAsset(entry.source,
				function(asset) {
					onSuccess(entry, asset);
					assets.splice(0, 1);
					loadAssets(assets, onSuccess, onError, onComplete);
				},
				function(error) {
					onError(error);
					assets.splice(0, 1);
					loadAssets(assets, onSuccess, onError, onComplete);
				});
		} else {
			onComplete();
		}
	}

	//------------------------------------------------------------------
	//
	// This function is used to asynchronously load image and audio assets.
	// On success the asset is provided through the onSuccess callback.
	// Reference: http://www.html5rocks.com/en/tutorials/file/xhr2/
	//
	//------------------------------------------------------------------
	function loadAsset(source, onSuccess, onError) {
		var xhr = new XMLHttpRequest(),
			asset = null,
			fileExtension = source.substr(source.lastIndexOf('.') + 1);	// Source: http://stackoverflow.com/questions/680929/how-to-extract-extension-from-filename-string-in-javascript

		if (fileExtension) {
			xhr.open('GET', source, true);
			xhr.responseType = 'blob';

			xhr.onload = function() {
				if (xhr.status === 200) {
					if (fileExtension === 'png' || fileExtension === 'jpg' || fileExtension === 'gif') {
						asset = new Image();
					} else if (fileExtension === 'mp3') {
						asset = new Audio();
					} else {
						if (onError) { onError('Unknown file extension: ' + fileExtension); }
					}
					asset.onload = function() {
						window.URL.revokeObjectURL(asset.src);
					};
					asset.src = window.URL.createObjectURL(xhr.response);
					if (onSuccess) { onSuccess(asset); }
				} else {
					if (onError) { onError('Failed to retrieve: ' + source); }
				}
			};
		} else {
			if (onError) { onError('Unknown file extension: ' + fileExtension); }
		}

		xhr.send();
	}

	//------------------------------------------------------------------
	//
	// Called when all the scripts are loaded, it kicks off the demo app.
	//
	//------------------------------------------------------------------
	function mainComplete() {
		console.log('Loading complete');
		MyGame.main.initialize();
	}

	//
	// Start with loading the assets, then the scripts.
	console.log('Starting to dynamically load project assets');
	loadAssets(assetOrder,
		function(source, asset) {	// Store it on success
			MyGame.assets[source.key] = asset;
		},
		function(error) {
			console.log(error);
		},
		function() {
			console.log('All audio and image assets loaded');
			console.log('Starting to dynamically load project scripts');
			loadScripts(scriptOrder, mainComplete);
		}
	);

}());
