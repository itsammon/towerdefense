'use strict';

function distance(x1, x2, y1, y2) {
    return Math.sqrt(Math.pow((x1-x2),2)+ Math.pow((y1-y2), 2));
}

function angle(x1, x2, y1, y2) {
    return Math.atan2(y2-y1, x2-x1);
}

function crossProduct2d(object1, object2) {
    return (object1.x*object2.y) - (object1.y*object2.x);
}

function computeAngle(rotation, object, target) {
    let v1 = {
        x: Math.cos(rotation),
        y: Math.sin(rotation)
    },
    v2 = {
        x: target.x - object.x,
        y: target.y - object.y
    },
    dp,
    angle;

    v2.len = Math.sqrt(v2.x*v2.x + v2.y*v2.y);
    v2.x /= v2.len;
    v2.y /= v2.len;

    dp = v1.x * v2.x + v1.y * v2.y;
    angle = Math.acos(dp);

    let cp = crossProduct2d(v1, v2);

    return {
        angle: angle,
        crossProduct: cp
    };
}

function testTolerance(value, test, tolerance) {
    if (Math.abs(value-test) < tolerance) {
        return true;
    } else {
        return false;
    }
}

function RectCircleColliding(circle,rect){
    var distX = Math.abs(circle.x - rect.x-rect.w/2);
    var distY = Math.abs(circle.y - rect.y-rect.h/2);

    if (distX > (rect.w/2 + circle.r)) { return false; }
    if (distY > (rect.h/2 + circle.r)) { return false; }

    if (distX <= (rect.w/2)) { return true; } 
    if (distY <= (rect.h/2)) { return true; }

    var dx=distX-rect.w/2;
    var dy=distY-rect.h/2;
    return (dx*dx+dy*dy<=(circle.r*circle.r));
}

let PlayArea = function () {
    let numCellsWide = 20;
    let numCellsHigh = 20;
    let cellWidth = 30;
    let cellHeight = 30;
    let playAreaXOffset = 50;
    let playAreaYOffset = 75;
    let gameTime = 0;
    let releaseMole = 1000;
    let newTower = {};
    let nextCreep = 1;
    let nextProjectile = 1;
    let nextTower = 1;
    let releaseNextLevel = false;
    let minLevelTimer = 0;
    let leftRightEntryCells = [{x: 0, y: Math.floor(numCellsHigh)/2-1}, {x: 0, y: Math.floor(numCellsHigh)/2}];
    let leftRightExitCells = [{x: numCellsWide-1, y: Math.floor(numCellsHigh)/2-1}, {x: numCellsWide-1, y: Math.floor(numCellsHigh)/2}];
    let topBottomEntryCells = [{x: Math.floor(numCellsWide)/2-1, y: 0}, {x: Math.floor(numCellsWide)/2, y: 0}];
    let topBottomExitCells = [{x: Math.floor(numCellsWide)/2-1, y: numCellsHigh-1}, {x: Math.floor(numCellsWide)/2, y: numCellsHigh-1}];
    let slowPercent = .5;
    let slowTime = 2;
    let waves = [
{ "numCreeps": 10, "type": "weak", "health": 10, "direction": "left->right", "speed": 25, "value": 1 }, 
{ "numCreeps": 15, "type": "weak", "health": 15, "direction": "left->right", "speed": 25, "value": 2 }, 
{ "numCreeps": 15, "type": "weak", "health": 25, "direction": "left->right", "speed": 25, "value": 3 }, 
{ "numCreeps": 10, "type": "weak", "health": 35, "direction": "top->bottom", "speed": 25, "value": 4 }, 
{ "numCreeps": 10, "type": "flying", "health": 50, "direction": "left->right", "speed": 30, "value": 5 }, 
{ "numCreeps": 10, "type": "strong", "health": 400, "direction": "left->right", "speed": 20, "value": 6 }, 
{ "numCreeps": 30, "type": "weak", "health": 60, "direction": "left->right", "speed": 25, "value": 7 }, 
{ "numCreeps": 10, "type": "strong", "health": 500, "direction": "top->bottom", "speed": 20, "value": 8 }, 
{ "numCreeps": 40, "type": "weak", "health": 10, "direction": "left->right", "speed": 40, "value": 9 }, 
{ "numCreeps": 10, "type": "flying", "health": 100, "direction": "left->right", "speed": 30, "value": 10 }, 
{ "numCreeps": 25, "type": "weak", "health": 80, "direction": "top->bottom", "speed": 25, "value": 11 }, 
{ "numCreeps": 30, "type": "weak", "health": 80, "direction": "left->right", "speed": 25, "value": 12 }, 
{ "numCreeps": 35, "type": "weak", "health": 90, "direction": "top->bottom", "speed": 25, "value": 13 }, 
{ "numCreeps": 50, "type": "weak", "health": 100, "direction": "left->right", "speed": 25, "value": 14 }, 
{ "numCreeps": 20, "type": "strong", "health": 1000, "direction": "left->right", "speed": 25, "value": 15 }
];

    var that = {
        numCellsWide: numCellsWide,
        numCellsHigh: numCellsHigh,
        cellWidth: cellWidth,
        cellHeight: cellHeight,
        playAreaXOffset: playAreaXOffset,
        playAreaYOffset: playAreaYOffset,
        buttons: [],
        cells: [],
        creeps: {},
        towers: {},
        waves: {},
        placeTower: false,
        towerToPlace: null,
        towerToPlaceType: null,
        selectedTower: null,
        level: 0,
        lives: 50,
        score: 0,
        money: 200,
        numCreeps: 0,
        gameOver: false,
    };

// -------------------------------------------------------------------------------------------
// Handle funds
// -------------------------------------------------------------------------------------------
    function addFunds(amount) {
        that.money += amount;
    }

    function removeFunds(amount) {
        if (amount > that.money) {
            return false;
        } else {
            that.money -= amount;
            return true;
        }
    }

// -------------------------------------------------------------------------------------------
// Handle score
// -------------------------------------------------------------------------------------------
    function addPoints(amount) {
        that.score += amount;
    }

// -------------------------------------------------------------------------------------------
// Cell
// -------------------------------------------------------------------------------------------

    function Cell(spec) {
        let that = {
            x: spec.x,
            y: spec.y,
            hasTower: false,
            tower: null,
            leftRightWeight: 0,
            topBottomWeight: 0,
            leftRightChecked: false,
            topBottomChecked: false,
        }

        that.reset = function() {
            if (!that.hasTower) {
                that.leftRightWeight = 0;
                that.topBottomWeight = 0;
            }
            that.leftRightChecked = false;
            that.topBottomChecked = false;
        }

        return that;
    }

// -----------------------------------------------------------------------------------------
// Projectile
// -----------------------------------------------------------------------------------------


    function damageCreeps(x, y, radius, damage, type, target, targetTypes, slows, sound) {
        let creep = 0;
        let circle = { x: x, y: y, r: radius };
        // Area of effect particles
        if (radius > 0) {
            for (creep in that.creeps) {
                if (targetTypes == 'ground' && that.creeps[creep].type !== 'flying') {
                    if (RectCircleColliding(circle, that.creeps[creep])) {
                        if (slows) {
                            that.creeps[creep].slowEffect(slowTime, slowPercent);
                        }
                        that.creeps[creep].damage(damage);
                    }
                } else if (targetTypes == 'flying' && that.creeps[creep].type === 'flying') {
                    if (RectCircleColliding(circle, that.creeps[creep])) {
                        if (slows) {
                            that.creeps[creep].slowEffect(slowTime, slowPercent);
                        }
                        that.creeps[creep].damage(damage);
                    }
                } else if (targetTypes == 'mixed') {
                    if (RectCircleColliding(circle, that.creeps[creep])) {
                        if (slows) {
                            that.creeps[creep].slowEffect(slowTime, slowPercent);
                        }
                        that.creeps[creep].damage(damage);
                    }
                }
            }
        } else {
            if (slows) {
                target.slowEffect(slowTime, slowPercent);
            }
            target.damage(damage);
        }
        // Handle particle effects and damage sounds
        Audio.playSound(sound);
        if (type === 'bomb') {
            ParticleSystem.createFireEffect({
                position: { x: x, y: y },
                duration: 0.3,
                speed: { mean: 150, stdev: 50 },
                lifetime: { mean: 0.5, stdev: 0.05 },
            });
            ParticleSystem.createSmokeEffect({
                position: { x: x, y: y },
                duration: 0.3,
                speed: { mean: 150, stdev: 50 },
                lifetime: { mean: 0.5, stdev: 0.05 },
            });
        } else if (type === 'ice') {
            ParticleSystem.createSmokeEffect({
                position: { x: x, y: y },
                duration: 0.3,
                speed: { mean: 150, stdev: 50 },
                lifetime: { mean: 0.5, stdev: 0.05 },
            });
        }
    }


    function Projectile(spec) {
        let that = {
            type: spec.type,
            x: spec.x,
            y: spec.y,
            width: spec.width,
            height: spec.height,
            image: spec.image,
            target: spec.target,
            damage: spec.damage,
            speed: spec.speed,
            rotation: spec.rotation,
            rotateRate: spec.rotateRate,
            damageRadius: spec.damageRadius,
            damageSound: spec.damageSound,
            targetTypes: spec.targetTypes,
            slows: spec.slows,
            slowPercent: .75,
            slowTime: 2,
        }

        that.update = function(elapsedTime) {
            if (that.target !== null) {
                if (that.type === 'missile') {
                    ParticleSystem.createSmokeEffect({
                        position: { x: that.x, y: that.y },
                        duration: 0.1,
                        speed: { mean: 0.4, stdev: 0.1 },
                        lifetime: { mean: 0.5, stdev: 0.05 },
                    });
                }

                let result = computeAngle(that.rotation, { x: that.x, y: that.y }, that.target);
                if (testTolerance(result.angle, 0, .01) === false) {
                    if (result.crossProduct > 0) {
                        that.rotation += that.rotateRate;
                    } else {
                        that.rotation -= that.rotateRate;
                    }
                }
                    
                let dist = distance(that.target.x, that.x, that.target.y, that.y);
                let xmove = that.speed*(elapsedTime/1000)*((that.target.x-that.x)/dist);
                let ymove = that.speed*(elapsedTime/1000)*((that.target.y-that.y)/dist);

                if (Math.abs(xmove) > Math.abs(that.x-that.target.x)) {
                    that.x = that.target.x;
                } else {
                    that.x += xmove;
                }
                if (Math.abs(ymove) > Math.abs(that.y-that.target.y)) {
                    that.y = that.target.y;
                } else {
                    that.y += ymove;
                }

                if (that.x == that.target.x && that.y == that.target.y) {
                    damageCreeps(that.x, that.y, that.damageRadius, that.damage, that.type, that.target, that.targetTypes, that.slows, spec.damageSound);
                    return true;
                }
                return false;
            }

            // Projectiles with no target just die
            return true;
        }

        return that;
    }

// ----------------------------------------------------------------------------------------------------
// Tower
// ----------------------------------------------------------------------------------------------------

    function Tower(spec) {
        let that = {
            type: spec.type,
            x: spec.x,
            y: spec.y,
            width: spec.width,
            height: spec.height,
            image: spec.image,
            // Default target is to the right of the tower
            target: null,
            range: spec.range,
            damage: spec.damage,
            cost: spec.cost,
            upgradeCost: spec.upgradeCost,
            rotation: spec.rotation,
            rotateRate: spec.rotateRate,
            fireArc: spec.fireArc,
            fireRate: spec.fireRate,
            projectileImage: spec.projectileImage,
            projectileSpeed: spec.projectileSpeed,
            damageRadius: spec.damageRadius,
            fireSound: spec.fireSound,
            timeSinceLastShot: 0,
            level: 1,
            maxLevel: spec.maxLevel,
            timeUpgrading: 0,
            upgradeTime: spec.upgradeTime, // In seconds
            upgrading: false,
            placeable: true,
            projectiles: {},
            towerSold: false,
            targetTypes: spec.targetTypes,
            slows: spec.slows,
        }

        function hasTowers(cell, cells) {
            for (let i = 0; i < that.width; i++) {
                for (let j = 0; j < that.height; j++) {
                    if (cells[cell.x+i][cell.y+j].hasTower) {
                        return true;
                    }
                }
            }
            return false;
        }

        function addTowerToCells(cells) {
            let cell = getCurrCell(that);
            if (cell != null) {
                for (let i = 0; i < that.width; i++) {
                    for (let j = 0; j < that.height; j++) {
                        cells[cell.x+i][cell.y+j].hasTower = true;
                        cells[cell.x+i][cell.y+j].tower = that;
                        cells[cell.x+i][cell.y+j].leftRightWeight = 10000;
                        cells[cell.x+i][cell.y+j].topBottomWeight = 10000;
                    }
                }
            }
            getPaths();
        }

        function removeTowerFromCells(cells) {
            let cell = getCurrCell({x: that.x, y: that.y});
            for (let i = 0; i < that.width; i++) {
                for (let j = 0; j < that.height; j++) {
                    cells[cell.x+i][cell.y+j].hasTower = false;
                    cells[cell.x+i][cell.y+j].reset();
                }
            }
            getPaths();
        }

        function fireShot() {
            // Create shot to fire and add it to the list of projectiles
            that.projectiles[nextProjectile++] = Projectile({
                type: that.type,
                x: that.x,
                y: that.y,
                width: spec.projectileSize.w,
                height: spec.projectileSize.h,
                image: that.projectileImage,
                target: that.target,
                damage: that.damage,
                speed: that.projectileSpeed,
                rotation: that.rotation,
                rotateRate: 20 * Math.PI /1000,
                damageRadius: that.damageRadius,
                damageSound: spec.damageSound,
                targetTypes: that.targetTypes,
                slows: that.slows,
            });
            that.timeSinceLastShot = 0;
            Audio.playSound(that.fireSound);
        }

        function blocksPaths(towerCell, cells) {
            let blocks = false;
            addTowerToCells(cells);
            for (let i = 0; i < leftRightEntryCells.length; i++){
                if (towerCell.x == leftRightEntryCells[i].x && towerCell.y == leftRightEntryCells[i].y) {
                    blocks = true;
                    break;
                }

                let nextCellLoc = getNextCellLoc(leftRightEntryCells[i], 'left->right');
                if (nextCellLoc == 'noPath' || nextCellLoc == 'endCell' || nextCellLoc == 'entryCell') {
                    blocks = true;
                    break;
                } 
            }
            for (let i = 0; i < topBottomEntryCells.length; i++){
                if (towerCell.x == topBottomEntryCells[i].x && towerCell.y == topBottomEntryCells[i].y) {
                    blocks = true;
                    break;
                }

                let nextCellLoc = getNextCellLoc(topBottomEntryCells[i], 'top->bottom');
                if (nextCellLoc == 'noPath' || nextCellLoc == 'endCell' || nextCellLoc == 'entryCell') {
                    blocks = true;
                    break;
                } 
            }
            for (let i = 0; i < leftRightExitCells.length; i++){
                if (towerCell.x == leftRightExitCells[i].x && towerCell.y == leftRightExitCells[i].y) {
                    blocks = true;
                    break;
                }
            }
            for (let i = 0; i < topBottomExitCells.length; i++){
                if (towerCell.x == topBottomExitCells[i].x && towerCell.y == topBottomExitCells[i].y) {
                    blocks = true;
                    break;
                }
            }
            removeTowerFromCells(cells);
            return blocks;
        }

        that.setLoc = function(position, cells) {
            let cell = getCurrCell(position);

            if (cell !== null) {
                // Convert to center point
                that.x = cell.x*cellWidth + playAreaXOffset + (that.width*cellWidth)/2;
                that.y = cell.y*cellHeight + playAreaYOffset + (that.height*cellHeight)/2;
                if (cell.x < numCellsWide-(that.width-1) && cell.x >= 0 && cell.y < numCellsHigh-(that.height-1) && cell.y >= 0) {
                    if (hasTowers(cell, cells) || blocksPaths(cell, cells)) {
                        that.placeable = false;
                    } else {
                        that.placeable = true;
                    } 
                } else {
                    that.placeable = false;
                }
            } else {
                that.placeable = false;
                that.x = position.x;
                that.y = position.y;
            }
        }

        that.placeTower = function(position, cells) {
            if (that.placeable && removeFunds(that.cost)) {
                that.setLoc(position, cells);
                addTowerToCells(cells);
                return true;
            }
            return false;
        }

        that.sellTower = function(cells) {
            removeTowerFromCells(cells);

            // Refund player
            addFunds(that.cost*0.5);
            
            // Remove tower
            that.towerSold = true;
        }

        that.getDrawWidth = function() {
            return that.width*cellWidth;
        }

        that.getDrawHeight = function() {
            return that.height*cellHeight;
        }

        that.upgrade = function() {
            if (!that.upgrading && that.level < that.maxLevel && removeFunds(that.upgradeCost)) {
                that.upgrading = true;
                that.level += 1;
                that.cost = that.cost + that.upgradeCost;
                that.damage = that.damage + spec.damagePerLevel;
                that.range = that.range + spec.rangePerLevel;
                that.fireRate = that.fireRate + spec.fireRatePerLevel;
                that.damageArea = that.damageArea + spec.damageAreaPerLevel;
                that.upgradeCost = that.upgradeCost + that.upgradeCost;
            }
        }

        that.setTarget = function(t) {
            that.target = t;
        }

        that.update = function(elapsedTime) {
            let removeMe = [];
            let projectile = null;
            let numProjectiles = 0;
            for (projectile in that.projectiles) {
                numProjectiles += 1;
                if (that.projectiles[projectile].update(elapsedTime)) {
                    removeMe.push(projectile);
                }
            }

            // Remove projectiles
            for (let projectile = 0; projectile < removeMe.length; projectile++) {
                delete that.projectiles[removeMe[projectile]];
            }

            that.timeSinceLastShot += elapsedTime/1000;
            if (that.towerSold) {
                return true;
            }

            if (that.upgrading) {
                that.timeUpgrading += elapsedTime/1000;
                if (that.upgradeTime < that.timeUpgrading) {
                    that.timeUpgrading = 0;
                    that.upgrading = false;
                }
                // Don't update targets or anything else while upgrading
                return false;
            }

            // If the current target is out of range, find the next target
            // Should cut down on cpu cycles
            if (that.target === null || that.target.health <= 0 || distance(that.x, that.target.x, that.y, that.target.y) > that.range) {
                let target = getTarget(that);
                that.setTarget(target);
            }

            // Get direction to target 
            if (that.target !== null) {
                let result = computeAngle(that.rotation, { x: that.x, y: that.y }, that.target);
                if (testTolerance(result.angle, 0, .02) === false) {
                    if (result.crossProduct > 0) {
                        that.rotation += that.rotateRate;
                    } else {
                        that.rotation -= that.rotateRate;
                    }
                }

                let dist = distance(that.x, that.target.x, that.y, that.target.y);
                if (that.timeSinceLastShot > (1/that.fireRate) && dist <= that.range && result.angle <= that.fireArc/2) {
                    // Fire a projectile
                    fireShot();
                }
            }
            return false;
        }

        return that;
    }

// ------------------------------------------------------------------------------------------
// Creep
// ------------------------------------------------------------------------------------------

    function Creep(spec) {
        let that = {
            x: spec.x,
            y: spec.y,
            w: spec.size,
            h: spec.size * spec.spriteRatio,
            direction: spec.direction,
            value: spec.value,
            health: spec.health,
            maxHealth: spec.maxHealth,
            timeAlive: 0,
            animationFrame: 0,
            type: spec.type,
            speed: spec.speed,
            rotation: 0,
            value: spec.value,
            exited: false,
            slowTime: 0, // In seconds
            currSpeedPercent: 1.0,
        }

        that.slowEffect = function(time, amount) {
            that.slowTime = time;
            that.currSpeedPercent = amount;
        }

        that.damage = function(damage) {
            that.health = that.health - damage;
        }

        that.update = function(elapsedTime) {
            that.timeAlive += elapsedTime;
            that.animationFrame = Math.floor(that.timeAlive/200 % 3);
            if (that.slowTime > 0) {
                that.slowTime = that.slowTime - elapsedTime/1000;
            } else if (that.slowTime <= 0) {
                that.currSpeedPercent = 1.0;
            }

            if (that.exited || that.type == 'flying') {
                if (that.direction == 'left->right') {
                    that.x = that.x + that.speed*(elapsedTime/1000)*that.currSpeedPercent;
                } else {
                    that.y = that.y + that.speed*(elapsedTime/1000)*that.currSpeedPercent;
                }
            } else {
                // Need to find the next cell to head to
                let currCell = getCurrCellLoc(that);
                let nextCell = getNextCellLoc(currCell, that.direction);
                if (nextCell === 'endCell' || nextCell === 'noPath') {
                    that.exited = true;
                    if (that.direction == 'left->right') {
                        that.x = that.x + that.speed*(elapsedTime/1000)*that.currSpeedPercent;
                        that.rotation = 0;
                    } else {
                        that.y = that.y + that.speed*(elapsedTime/1000)*that.currSpeedPercent;
                        that.rotation = Math.PI/2;
                    }
                } else {
                    let destinationX = playAreaXOffset + nextCell.x*cellWidth+cellWidth/2;
                    let destinationY = playAreaYOffset + nextCell.y*cellHeight+cellHeight/2;
                    let dist = distance(destinationX, that.x, destinationY, that.y);
                    that.rotation = angle(that.x, destinationX, that.y, destinationY);
                    that.x = that.x + that.speed*(elapsedTime/1000)*((destinationX-that.x)/dist)*that.currSpeedPercent;
                    that.y = that.y + that.speed*(elapsedTime/1000)*((destinationY-that.y)/dist)*that.currSpeedPercent;
                }
            }

            if (that.x > playAreaXOffset + numCellsWide*cellWidth+25 
                || that.y > playAreaYOffset + numCellsHigh*cellHeight + 25) {
                    removeLife();
                    return true;
            } else if (that.health <= 0) {
                // Need to handle death in particle system
                addFunds(that.value);
                addPoints(that.value);
                Audio.playSound('creepDeath.mp3');
                ParticleSystem.createTextEffect({
                        position: {x: that.x, y: that.y},
                        text: that.value,
                        duration: 1,
                        lifetime: 2,
                    });
                return true;
            } else {
                return false;
            }
        }
        return that;
    }

// ------------------------------------------------------------------------------------------------
// Button for UI
// ------------------------------------------------------------------------------------------------
    function Button(spec) {
        let that = {
            action: spec.action,
            image: spec.image,
            bkgdImage: spec.bkgdImage,
            selected: false,
            x: spec.x,
            y: spec.y,
            w: spec.w,
            h: spec.h,
            text: spec.text,
            spriteDimensions: spec.spriteDimensions,
        };

        that.click = function() {
            that.action();
        }

        return that;
    }

// ------------------------------------------------------------------------------------------------
// Wave code
// ------------------------------------------------------------------------------------------------
    function Wave(spec) {
        let that = {
            numCreeps: spec.numCreeps,
            releasedCreeps: 0,
            releaseTimer: 0,
            creepFrequency: 1, // In seconds
        };

        that.update = function(elapsedTime) {
            
            that.releaseTimer += (elapsedTime / 1000);

            if (that.releasedCreeps < that.numCreeps && that.releaseTimer >= that.creepFrequency) {
                that.releaseTimer = that.releaseTimer - that.creepFrequency;
                createCreeps(1, spec.type, spec.health, spec.direction, spec.speed, spec.value);
                that.releasedCreeps += 1;
            }
            if (that.releasedCreeps >= that.numCreeps) {
                return true;
            }
            return false;
        }

        return that;
    }

// ------------------------------------------------------------------------------------------------
// Pathfinding and object locating
// ------------------------------------------------------------------------------------------------

    // Function to get the nearest target for towers
    function getTarget(object) {
        let creep = null;
        let dist = 1000000;
        let shortestDist = 100000;
        let target = null;
        for (creep in that.creeps) {
            dist = distance(object.x, that.creeps[creep].x, object.y, that.creeps[creep].y);
            if (dist < shortestDist) {
                if (object.targetTypes == 'ground' && that.creeps[creep].type !== 'flying') {
                    shortestDist = dist;
                    target = that.creeps[creep];
                } else if (object.targetTypes == 'flying' && that.creeps[creep].type === 'flying') {
                    shortestDist = dist;
                    target = that.creeps[creep];
                } else if (object.targetTypes == 'mixed') {
                    shortestDist = dist;
                    target = that.creeps[creep];
                }
            }
        }
        return target;
    }

    function getCurrCell(object) {
        let loc = { x:0, y:0 };
        if (object.x < playAreaXOffset) {
            loc.x = -1;
        } else {
            loc.x = Math.floor((object.x-playAreaXOffset)/cellWidth);
        }
        if (object.y < playAreaYOffset) {
            loc.y = -1;
        } else {
            loc.y = Math.floor((object.y-playAreaYOffset)/cellHeight);
        }

        if (loc.x >= 0 && loc.y >= 0 && loc.x < numCellsWide && loc.y < numCellsHigh) {
            return that.cells[loc.x][loc.y];
        }
        else {
            return null;
        }
    }

    function getTower(cell) {
        if (cell != null && cell.hasTower) {
            return cell.tower;
        } 
        return null;
    }

    function getCurrCellLoc(object) {
        let loc = { x:0, y:0 };
        if (object.x < playAreaXOffset) {
            loc.x = -1;
        } else {
            loc.x = Math.floor((object.x-playAreaXOffset)/cellWidth);
        }
        if (object.y < playAreaYOffset) {
            loc.y = -1;
        } else {
            loc.y = Math.floor((object.y-playAreaYOffset)/cellHeight);
        }

        return loc;
    }

    function getNextCellLoc(cellLoc, direction) {
        if (cellLoc.x < 0 || cellLoc.y < 0) {
            return getClosestEntryCell(cellLoc, direction);
        }

        if (direction == 'left->right') {
            for (let i = 0; i < leftRightExitCells.length; i++){
                if (cellLoc.x == leftRightExitCells[i].x && cellLoc.y == leftRightExitCells[i].y) {
                    return 'endCell';
                }
            }
        } else {
            for (let i = 0; i < topBottomExitCells.length; i++){
                if (cellLoc.x == topBottomExitCells[i].x && cellLoc.y == topBottomExitCells[i].y) {
                    return 'endCell'
                }
            }
        }
        return getLowest(cellLoc, direction);
    }

    function getClosestEntryCell(loc, direction) {
        let shortestDist = 1000000;
        let dist;
        let cell = null;
        if (direction == 'left->right') {
            cell = leftRightEntryCells[0];
            for (let i = 0; i < leftRightEntryCells.length; i++) {
                dist = distance(leftRightEntryCells[i].x, loc.x, leftRightEntryCells[i].y, loc.y);
                if (dist < shortestDist) {
                    cell = leftRightEntryCells[i];
                    shortestDist = dist;
                }
            }
        } else {
            cell = topBottomEntryCells[0];
            for (let i = 0; i < topBottomEntryCells.length; i++) {
                dist = distance(topBottomEntryCells[i].x, loc.x, topBottomEntryCells[i].y, loc.y);
                if (dist < shortestDist) {
                    cell = topBottomEntryCells[i];
                    shortestDist = dist;
                }
            }
        }
        return cell;
    }

    function getLowest(loc, direction) {
        let lowestWeight = 1000000;
        let lowest = { x:-1, y:-1}
        let x = loc.x-1;
        let y = loc.y;
        let weight = isLowest(x, y, lowestWeight, direction);
        if (weight < lowestWeight) {
            lowestWeight = weight;
            lowest.x = x;
            lowest.y = y;
        }
        x = loc.x+1;
        weight = isLowest(x, y, lowestWeight, direction);
        if (weight < lowestWeight) {
            lowestWeight = weight;
            lowest.x = x;
            lowest.y = y;
        }
        x = loc.x;
        y = loc.y-1;
        weight = isLowest(x, y, lowestWeight, direction);
        if (weight < lowestWeight) {
            lowestWeight = weight;
            lowest.x = x;
            lowest.y = y;
        }
        y = loc.y+1;
        weight = isLowest(x, y, lowestWeight, direction);
        if (weight < lowestWeight) {
            lowestWeight = weight;
            lowest.x = x;
            lowest.y = y;
        }

        // If the weight is too large, there is no path
        if (lowestWeight > 100 || lowestWeight <= 0) {
            return 'noPath';
        } else {
            return lowest;
        } 
    }

    function isLowest(x, y, lowestWeight, direction) {
        if (direction == 'left->right') {
            if (x >= 0 && y >= 0 && x < that.numCellsWide && y < that.numCellsHigh) {
                if (that.cells[x][y].leftRightWeight < lowestWeight) {
                    return that.cells[x][y].leftRightWeight;
                }
            }
        } else {
            if (x >= 0 && y >= 0 && x < that.numCellsWide && y < that.numCellsHigh) {
                if (that.cells[x][y].topBottomWeight < lowestWeight) {
                    return that.cells[x][y].topBottomWeight;
                }
            }
        }
        return lowestWeight;
    }


    function getPaths() {
        for (let x = 0; x < that.numCellsWide; x++) {
            for (let y = 0; y < that.numCellsHigh; y++) {
                that.cells[x][y].reset();
            }
        }
        getRightLeftPath();
        getTopBottomPath();
    }

    function getRightLeftPath() {
        let cellsToCheck = [];
        let currCell = null;
        for (let i = 0; i < leftRightExitCells.length; i++) {
            cellsToCheck.push(that.cells[leftRightExitCells[i].x][leftRightExitCells[i].y]);
            that.cells[leftRightExitCells[i].x][leftRightExitCells[i].y].leftRightChecked = true;
        }
        while(cellsToCheck.length > 0) {
            currCell = cellsToCheck.shift();
            if (currCell.x-1 > -1 && !that.cells[currCell.x-1][currCell.y].leftRightChecked && that.cells[currCell.x-1][currCell.y].hasTower == false) {
                that.cells[currCell.x-1][currCell.y].leftRightWeight = (currCell.leftRightWeight+1);
                that.cells[currCell.x-1][currCell.y].leftRightChecked = true;
                cellsToCheck.push(that.cells[currCell.x-1][currCell.y]);
            }
            if (currCell.x+1 < that.numCellsWide && !that.cells[currCell.x+1][currCell.y].leftRightChecked && that.cells[currCell.x+1][currCell.y].hasTower == false) {
                that.cells[currCell.x+1][currCell.y].leftRightWeight = (currCell.leftRightWeight+1);
                that.cells[currCell.x+1][currCell.y].leftRightChecked = true;
                cellsToCheck.push(that.cells[currCell.x+1][currCell.y]);
            }
            if (currCell.y-1 > -1 && !that.cells[currCell.x][currCell.y-1].leftRightChecked && that.cells[currCell.x][currCell.y-1].hasTower == false) {
                that.cells[currCell.x][currCell.y-1].leftRightWeight = (currCell.leftRightWeight+1);
                that.cells[currCell.x][currCell.y-1].leftRightChecked = true;
                cellsToCheck.push(that.cells[currCell.x][currCell.y-1]);
            }
            if (currCell.y+1 < that.numCellsHigh && !that.cells[currCell.x][currCell.y+1].leftRightChecked && that.cells[currCell.x][currCell.y+1].hasTower == false) {
                that.cells[currCell.x][currCell.y+1].leftRightWeight = (currCell.leftRightWeight+1);
                that.cells[currCell.x][currCell.y+1].leftRightChecked = true;
                cellsToCheck.push(that.cells[currCell.x][currCell.y+1]);
            }
        }
    }

    function getTopBottomPath() {
        let cellsToCheck = [];
        let currCell = null;
        for (let i = 0; i < topBottomExitCells.length; i++) {
            cellsToCheck.push(that.cells[topBottomExitCells[i].x][topBottomExitCells[i].y]);
            that.cells[topBottomExitCells[i].x][topBottomExitCells[i].y].topBottomChecked = true;
        }
        while(cellsToCheck.length > 0) {
            currCell = cellsToCheck.shift();
            if (currCell.x-1 > -1 && !that.cells[currCell.x-1][currCell.y].topBottomChecked && that.cells[currCell.x-1][currCell.y].hasTower == false) {
                that.cells[currCell.x-1][currCell.y].topBottomWeight = (currCell.topBottomWeight+1);
                that.cells[currCell.x-1][currCell.y].topBottomChecked = true;
                cellsToCheck.push(that.cells[currCell.x-1][currCell.y]);
            }
            if (currCell.x+1 < that.numCellsWide && !that.cells[currCell.x+1][currCell.y].topBottomChecked && that.cells[currCell.x+1][currCell.y].hasTower == false) {
                that.cells[currCell.x+1][currCell.y].topBottomWeight = (currCell.topBottomWeight+1);
                that.cells[currCell.x+1][currCell.y].topBottomChecked = true;
                cellsToCheck.push(that.cells[currCell.x+1][currCell.y]);
            }
            if (currCell.y-1 > -1 && !that.cells[currCell.x][currCell.y-1].topBottomChecked && that.cells[currCell.x][currCell.y-1].hasTower == false) {
                that.cells[currCell.x][currCell.y-1].topBottomWeight = (currCell.topBottomWeight+1);
                that.cells[currCell.x][currCell.y-1].topBottomChecked = true;
                cellsToCheck.push(that.cells[currCell.x][currCell.y-1]);
            }
            if (currCell.y+1 < that.numCellsHigh && !that.cells[currCell.x][currCell.y+1].topBottomChecked && that.cells[currCell.x][currCell.y+1].hasTower == false) {
                that.cells[currCell.x][currCell.y+1].topBottomWeight = (currCell.topBottomWeight+1);
                that.cells[currCell.x][currCell.y+1].topBottomChecked = true;
                cellsToCheck.push(that.cells[currCell.x][currCell.y+1]);
            }
        }
    }

// ----------------------------------------------------------------------------------------------------
// Create parts for the play area
// ----------------------------------------------------------------------------------------------------

    function createCells() {
        for (let x = 0; x < that.numCellsWide; x++) {
            that.cells[x] = [];
            for (let y = 0; y < that.numCellsHigh; y++) {
                that.cells[x][y] = Cell({
                    x: x,
                    y: y
                });
            }
        }
    }

    function createCreeps(numCreeps, type, health, direction, speed, value) {
        let spriteRatio = 0;
        if (type === 'weak') {
            spriteRatio = (250/500);
        } else if (type === 'strong') {
            spriteRatio = (250/500);
        } else if (type === 'flying') {
            spriteRatio = (450/500);
        }

        if (direction == 'left->right') {
            for (let i = 0; i < numCreeps; i++) {
                that.creeps[nextCreep++] = Creep({
                    x: 0,
                    y: playAreaYOffset + Math.floor(numCellsHigh/2-2)*cellHeight + Math.floor(Math.random()*cellHeight*4),
                    size: 50,
                    spriteRatio: spriteRatio,
                    value: value,
                    direction: direction,
                    health: health,
                    maxHealth: health,
                    type: type,
                    speed: speed,
                });
            }
        } else {
            for (let i = 0; i < numCreeps; i++) {
                that.creeps[nextCreep++] = Creep({
                    x: playAreaXOffset + Math.floor(numCellsWide/2-2)*cellWidth + Math.floor(Math.random()*cellWidth*4) ,
                    y: 0,
                    size: 50,
                    spriteRatio: spriteRatio,
                    value: value,
                    direction: direction,
                    health: health,
                    maxHealth: health,
                    type: type,
                    speed: speed,
                });
            }
        }
    }

    function createProjectileTower(position) {
        let tower = Tower({
            type: 'bullet',
            x: 0,
            y: 0,
            width: 1,
            height: 1,
            image: 'projectileTower',
            range: 100,
            damage: 25,
            cost: 10,
            upgradeCost: 50,
            maxLevel: 3,
            rotation: 0,
            rotateRate: 10 * Math.PI / 1000, //Radians per second
            fireArc: 0.4,
            fireRate: 2, // In seconds
            fireSound: 'bulletSound.mp3',
            damageRadius: 0,
            damageSound: null,
            //damageSound: 'damageSound.mp3',
            projectileImage: 'bullet',
            projectileSize: { w: 10, h: 10},
            projectileSpeed: 200,
            upgradeTime: 5, // In seconds
            damagePerLevel: 25,
            rangePerLevel: 50,
            fireRatePerLevel: 0.5, // Down per second
            damageAreaPerLevel: 0,
            targetTypes: 'mixed',
            slows: false,
        });
        return tower;
    }

    function createBombTower(position) {
        let tower = Tower({
            type: 'bomb',
            x: 0,
            y: 0,
            width: 1,
            height: 1,
            image: 'bombTower',
            range: 200,
            damage: 150,
            cost: 50,
            upgradeCost: 75,
            maxLevel: 3,
            rotation: 0,
            rotateRate: 10 * Math.PI / 1000, //Radians per second
            fireArc: 0.4,
            fireRate: .25, // In seconds
            fireSound: 'shot.mp3',
            damageRadius: 50,
            damageSound: 'bombExplosion.mp3',
            projectileImage: 'bomb',
            projectileSize: { w: 50, h: 50},
            projectileSpeed: 150,
            upgradeTime: 5, // In seconds
            damagePerLevel: 20,
            rangePerLevel: 50,
            fireRatePerLevel: 0.1,
            damageAreaPerLevel: 10,
            targetTypes: 'ground',
            slows: false,
        });
        return tower;
    }

    function createMissileTower(position) {
        let tower = Tower({
            type: 'missile',
            x: 0,
            y: 0,
            width: 1,
            height: 1,
            image: 'missileTower',
            range: 150,
            damage: 200,
            cost: 25,
            upgradeCost: 50,
            maxLevel: 3,
            rotation: 0,
            rotateRate: 10 * Math.PI / 1000, //Radians per second
            fireArc: 0.4,
            fireRate: .5, // In seconds
            fireSound: 'shot.mp3',
            damageRadius: 0,
            damageSound: 'missileExplosion.mp3',
            projectileImage: 'missile',
            projectileSize: { w: 25, h: 25},
            projectileSpeed: 150,
            upgradeTime: 5, // In seconds
            damagePerLevel: 20,
            rangePerLevel: 100,
            fireRatePerLevel: 0.25,
            damageAreaPerLevel: 0,
            targetTypes: 'flying',
            slows: false,
        });
        return tower;
    }

    function createIceTower(position) {
        let tower = Tower({
            type: 'ice',
            x: 0,
            y: 0,
            width: 1,
            height: 1,
            image: 'iceTower',
            range: 75,
            damage: 25,
            cost: 100,
            upgradeCost: 50,
            maxLevel: 3,
            rotation: 0,
            rotateRate: 10 * Math.PI / 1000, //Radians per second
            fireArc: 0.4,
            fireRate: 0.5, // In seconds
            fireSound: null,
            damageRadius: 10,
            damageSound: 'iceSound.mp3',
            projectileImage: 'ice',
            projectileSize: { w: 25, h: 25},
            projectileSpeed: 100,
            upgradeTime: 5, // In seconds
            damagePerLevel: 25,
            rangePerLevel: 50,
            fireRatePerLevel: 0.1,
            damageAreaPerLevel: 5,
            targetTypes: 'ground',
            slows: true,
        });
        return tower;
    }

    function createTower(type, position) {
        let tower = newTower[type](position);

        if (tower.placeTower(position, that.cells)) {
            that.towers[nextTower++] = tower;
        } else {
            console.log('Not enough funds');
            // Play sound to notify that tower costs too much
        }
    }

    function createButtons() {
        let button = Button({
            action: function() {
                that.towerToPlace = createProjectileTower({x: 0, y: 0});
                that.placeTower = true;
                that.towerToPlaceType = 'projectile';
                that.selectedTower = that.towerToPlace;
            },
            image: 'projectileTower',
            bkgdImage: 'towerBase',
            x: 700,
            y: playAreaYOffset,
            w: 50,
            h: 50,
            text: null,
            spriteDimensions: { x: 1000, y: 1000 },
        });
        that.buttons.push(button);
        
        button = Button({
            action: function() {
                that.towerToPlace = createBombTower({x: 0, y: 0});
                that.placeTower = true;
                that.towerToPlaceType = 'bomb';
                that.selectedTower = that.towerToPlace;
            },
            image: 'bombTower',
            bkgdImage: 'towerBase',
            x: 775,
            y: playAreaYOffset,
            w: 50,
            h: 50,
            text: null,
            spriteDimensions: { x: 1000, y: 1000 },
        });
        that.buttons.push(button);
        button = Button({
            action: function() {
                that.towerToPlace = createMissileTower({x: 0, y: 0});
                that.placeTower = true;
                that.towerToPlaceType = 'missile';
                that.selectedTower = that.towerToPlace;
            },
            image: 'missileTower',
            bkgdImage: 'towerBase',
            x: 850,
            y: playAreaYOffset,
            w: 50,
            h: 50,
            text: null,
            spriteDimensions: { x: 1000, y: 1000 },
        });
        that.buttons.push(button);
        button = Button({
            action: function() {
                that.towerToPlace = createIceTower({x: 0, y: 0});
                that.placeTower = true;
                that.towerToPlaceType = 'ice';
                that.selectedTower = that.towerToPlace;
            },
            image: 'iceTower',
            bkgdImage: 'towerBase',
            x: 925,
            y: playAreaYOffset,
            w: 50,
            h: 50,
            text: null,
            spriteDimensions: { x: 1000, y: 1000 },
        });
        that.buttons.push(button);
        button = Button({
            action: function() {
                that.nextLevel();
            },
            image: null,
            bkgdImage: null,
            text: 'Next Wave',
            x: 700,
            y: playAreaYOffset + 550,
            w: 75,
            h: 50,
        });
        that.buttons.push(button);
        button = Button({
            action: function() {
                that.sellTower();
            },
            image: null,
            bkgdImage: null,
            text: 'Sell',
            x: 800,
            y: playAreaYOffset + 550,
            w: 75,
            h: 50,
        });
        that.buttons.push(button);
        button = Button({
            action: function() {
                that.upgrade();
            },
            image: null,
            bkgdImage: null,
            text: 'Upgrade',
            x: 900,
            y: playAreaYOffset + 550,
            w: 75,
            h: 50,
        });
        that.buttons.push(button);
    }

// ----------------------------------------------------------------------------------
// Core playArea functions
// ----------------------------------------------------------------------------------

    function gameOver() {
        var person = prompt("Please enter your name");
        console.log(person);
        var name = '';
        if (person == null || person == "") {
            name = 'John Doe';
        } else {
            name = person;
        }

        console.log(name, that.score);
        MyGame.highscores.addHighScore(name, that.score);
        that.gameOver = true;
    }

    function removeLife() {
        that.lives = that.lives - 1;
        if (that.lives <= 0) {
            gameOver();
        }
    }

    that.isWin = function() {
        if (that.level >= waves.length && that.numCreeps <= 0 && Object.keys(that.waves).length <= 0) {
            return true;
        } else {
            return false;
        }
    }

    that.nextLevel = function() {
        if (that.level < waves.length && that.level >= 0) {
            that.waves[that.level] = Wave(waves[that.level]);
            that.level += 1;
        }
    }

    that.upgrade = function() {
        if (that.selectedTower !== null) {
            that.selectedTower.upgrade();
        }
    }

    that.sellTower = function() {
        if (that.selectedTower !== null) {
            that.selectedTower.sellTower(that.cells);
            that.selectedTower = null;
        }
    }

    that.mouseDown = function(position) {
        let gamespace = {
            x: playAreaXOffset,
            y: playAreaYOffset,
            w: numCellsWide*cellWidth,
            h: numCellsHigh*cellHeight,
        }

        // Used for checking click location
        let circle = {
            x: position.x,
            y: position.y,
            r: 0,
        }

        if (RectCircleColliding(circle, gamespace) && that.placeTower && that.towerToPlace !== null && that.towerToPlace.placeable) {
            that.selectedTower = createTower(that.towerToPlaceType, position);
            that.placeTower = false;
            that.towerToPlace = null;
        } 
        for (let i = 0; i < that.buttons.length; i++) {
            if (RectCircleColliding(circle, that.buttons[i])) {
                that.buttons[i].click();
            } else {
                that.buttons[i].selected = false;
            }
        }

        if (that.towerToPlace !== null) {
            that.towerToPlace.setLoc(position, that.cells);
        }
        if (RectCircleColliding(circle, gamespace)) {
            that.selectedTower = getTower(getCurrCell(position));
        }
    }

    that.mouseOver = function(position) {
        if (that.placeTower && that.towerToPlace !== null) {
            that.towerToPlace.setLoc(position, that.cells);
        }

        // Used for checking if over a button
        let circle = {
            x: position.x,
            y: position.y,
            r: 0,
        }
        for (let i = 0; i < that.buttons.length; i++) {
            if (RectCircleColliding(circle, that.buttons[i])) {
                that.buttons[i].selected = true;
            } else {
                that.buttons[i].selected = false;
            }
        }
    }

    that.update = function(elapsedTime) {
        gameTime = gameTime + elapsedTime;
        let removeMe = [];
        let creep = 0;
        let tower = 0;
        let wave = 0;
        let numTowers = 0;
        that.numCreeps = 0;

        // If the game is over, pause the game and let the renderer display the game over
        if (that.lives <= 0) {
            return;
        }

        for (creep in that.creeps) {
            that.numCreeps += 1;
            if (that.creeps[creep].update(elapsedTime)) {
                removeMe.push(creep);
            }
        }

        // Remove creeps
        for (creep = 0; creep < removeMe.length; creep++) {
            delete that.creeps[removeMe[creep]];
        }
        removeMe.length = 0;

        // Handle waves that are currently active
        for(wave in that.waves) {
            if (that.waves[wave].update(elapsedTime)) {
                removeMe.push(wave);
            }
        }

        // Remove waves
        for (wave = 0; wave < removeMe.length; wave++) {
            delete that.waves[removeMe[wave]];
        }
        removeMe.length = 0;

        for (tower in that.towers) {
            numTowers += 1;
            if (that.towers[tower].update(elapsedTime)) {
                removeMe.push(tower);
            }
        }

        // Remove towers
        for (tower = 0; tower < removeMe.length; tower++) {
            delete that.towers[removeMe[tower]];
        }
        removeMe.length = 0;

        // Check win condition
        if (!that.gameOver) {
            if (that.numCreeps <= 0 && that.level >= waves.length && Object.keys(that.waves).length <= 0) {
                gameOver();
            }
        }
    }

    that.newGame = function() {
        gameTime = 0;
        let tower = null;

        // Reset the creeps and cells
        for (tower in that.towers) {
            that.towers[tower].sellTower(that.cells);
        }
        that.towers = {};
        that.creeps = {};
        that.waves = {};
        that.selectedTower = null;

        getPaths();

        // Initialize player starting cash and lives
        // Reset the score
        that.lives = 50;
        that.money = 50;
        that.score = 0;
        that.level = 0;
        that.gameOver = false;
    }

    that.initialize = function(input) {
        createCells();

        // Setup tower types
        newTower['projectile'] = createProjectileTower;
        newTower['bomb'] = createBombTower;
        newTower['missile'] = createMissileTower;
        newTower['ice'] = createIceTower;

        // TODO: Setup waves

        // Create the button interface
        createButtons();

        //that.towerToPlace = createTestTower({x: 0, y: 0});
        //that.towerToPlaceType = 'test';

        // Initialize Pathfinding
        that.newGame();
    }

    return that;
}