var express = require('express');
var app = express();
var path = require("path");
var http = require('http');
var highscores = require('./public/highscores');

app.set('port', process.env.PORT || 3000);

app.use('/styles', express.static(path.join(__dirname+'/styles')));
app.use('/scripts', express.static(path.join(__dirname+'/scripts')));
app.use('/images', express.static(path.join(__dirname+'/images')));
app.use('/audio', express.static(path.join(__dirname+'/audio')));
app.use('/json', express.static(path.join(__dirname+'/json')));

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname+'/index.html'));
})

app.get('/hs/highscores', highscores.all);
app.post('/hs/highscores', highscores.add);

app.all('/public/*', function(request, response) {
  response.writeHead(501);
  response.end();
});

http.createServer(app).listen(app.get('port'), function() {
  console.log('Server listening on port ' + app.get('port'));
})